// Code for the HD44780 LCD controller
// See details at http://www.toughdev.com/2011/02/experimenting-with-lcd-modules-and-pic.html
//
// References:
//
// http://www.8052.com/tutlcd
// http://www.botskool.com/user-pages/tutorials/electronics/simple-8-bit-16x2-lcd-interfacing-pic-16f
// Tested with PIC16LF84A.
// PORTB: Data pin (D0..D7);
// RA2: Enable (E)
// RA1: R/W
// RA0: RS
// RA3: LCD Type (High = 20x4, Low = 16x2)

#include "pic.h"

void command(); //function for command manipolation
void data(); //function for data manipulation
void DelayUs(int D);
void DelayMs(int D);
void goLine1();
void goLine2();
void goLine3();
void goLine4();


void main()
{
	char a[] = "LCD TESTING DEMO";
	
	int i;
	TRISB=0x00; //Made Port B Out put port (to use with in main function)
	TRISA=0b00001000; //RA2,RA1,RA0 output. RA3 input;

	int CHAR_PER_LINE;
	int TOTAL_LINE;
	
	DelayMs(20);
	
	//define LCD type
	if (RA3 == 1)
		{
			CHAR_PER_LINE = 20;
			TOTAL_LINE = 4;
		}
		else
		{
			CHAR_PER_LINE = 16;
			TOTAL_LINE = 2;
		}			
		
	PORTB=0x38; //to select the mode of the LED 0X38 - 8 bit 2 lines
	command(); // Call command function, shows that the given values are commands and not datas
	DelayMs(1);	
	
	PORTB=0x0E; // turn the cursor on. 
	command();
	DelayMs(1);
	
	PORTB=0x0F; // the cursor position automatically moves to the right, blinking
	command();
	DelayMs(1);
	
	// write first line
	goLine1();
	for(i=0;i<16;i++)	
	{
		PORTB = a[i];
		data(); //To show that the given values are data
		DelayMs(1);
	}
		
	// remaining chars of line 1
	int remain;
	remain = CHAR_PER_LINE - sizeof(a);
	for(i=0;i<remain;i++)	
	{
		PORTB = '*';
		data(); //To show that the given values are data
		DelayMs(1);
	}
				
	// write placeholder for remaining line
	if (TOTAL_LINE > 2)
	{
			for (int k=3; k <= TOTAL_LINE; k++)
			{
				 if (k == 3) goLine3(); else goLine4();
				 	
				 for(i=0;i<CHAR_PER_LINE;i++)	
				 {
				 	  PORTB = (k % 2 == 0 ? '#' : '-');
				 		data(); //To show that the given values are data
				 		DelayMs(1);
				 }
			}
	}
	
	// second line with all normal ASCII character (32-126)
	goLine2();	
	int k;	
	int total;
	total = 0;
	while(1)
	{			
		PORTB = 32 + (k % 95);
		data(); 
		DelayMs(1);
		k++;	
		total++;
		
    // line feed
		if (total % CHAR_PER_LINE == 0) goLine2();
	}
}

void goLine1()
{
   PORTB = 0x80;	
	 command();
	 DelayMs(5);
}

void goLine2()
{
   PORTB = 0xC0;	
	 command();
	 DelayMs(5);
}

void goLine3()
{
   PORTB = 0x94;	
	 command();
	 DelayMs(5);
}

void goLine4()
{
   PORTB = 0xD4;	
	 command();
	 DelayMs(5);
}

void command() //command function definition
{
	RA0=0;
	RA1=0;
	RA2=1;
	DelayUs(1);
	RA2=0;
}

void data()  //data function definition
{
	RA0=1;
	RA1=0;
	RA2=1;
	DelayUs(1);
	RA2=0;
}

void DelayUs(int D)
{
	for (int i=0; i<1*D; i++);	
}

void DelayMs(int D)
{
	for (int i=0; i<1000*D; i++);	
}
