/*
Code to convert an Outlook Form Template (OFT) to a VB6 form.

See details at http://www.toughdev.com/2013/07/converting-outlook-form-template-oft-to.html
*/
using Microsoft.Vbe.Interop.Forms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Office = Microsoft.Office.Core;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Reflection;
using OutlookAddIn1.Properties;
using System.Diagnostics;
using Microsoft.Office.Tools.Outlook;


namespace OutlookAddIn1
{
    public static class Outlook_to_VB6
    {

        private static int boolToVbBool(bool val)
        {
            return val ? -1 : 0; // true in VB is -1!
        }

        const string fontTemplate = @"
        BeginProperty Font 
            Name            =   ""{0}""
            Size            =   {1}
            Charset         =   {2}
            Weight          =   {3}
            Underline       =   {4}
            Italic          =   {5}
            Strikethrough   =   {6}
        EndProperty";

        private static string getFontString(dynamic font)
        {
            try
            {
                string fontStr = String.Format(fontTemplate, font.Name, font.Size, font.Charset, font.Weight, boolToVbBool(font.Underline), boolToVbBool(font.Italic), boolToVbBool(font.Strikethrough));
                return fontStr;
            }
            catch
            {
                return "";
            }
        }

        public static string escapeString(string s)
        {
            return s.Replace(Environment.NewLine, "").Replace("\"", "");
        }

        public static int toVBTextAlign(int oldAlign)
        {
            switch (oldAlign)
            {
                case 2: //center
                    return 2;
                case 3: //right
                    return 1;
                default: //left
                    return 0;
            }
        }

        public static Dictionary<String, Control> addedControls = new Dictionary<String, Control>();

        public static void exportFromRoot(Control ctl, ref int count, ref string strControlCode)
        {
            if (addedControls.ContainsKey(ctl.Name))
            {
                Debug.WriteLine("Already added control: " + ctl.Name + " " + ctl.GetType().Name);
                return;
            }
            else
            {
                addedControls.Add(ctl.Name, ctl);
            }

            if (ctl is Microsoft.Vbe.Interop.Forms.UserForm)
            {
                UserForm frame = (UserForm)ctl;

                string frameChildren = "";
                foreach (Control subCtl in frame.Controls)
                {
                    exportFromRoot(subCtl, ref count, ref frameChildren);
                }

                // add indentation for the sub control
                string identedFrameChildren = "";
                int i = 0;
                foreach (string line in frameChildren.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (i>0)
                        identedFrameChildren += "    " + line + Environment.NewLine;
                    else
                        identedFrameChildren += line + Environment.NewLine;

                    i++;
                }

                string frameTemplate = String.Format(@"  
    Begin VB.Frame {0} 
        Caption         =   ""{1}""
        Height          =   {2}
        Left            =   {3}
        TabIndex        =   {4}
        Top             =   {5}
        Width           =   {6}
        BackColor       =   &H{7}&
        ForeColor       =   &H{8}&
        {9}
        {10}
    End",
                    ctl.Name.Replace(" ",""), frame.Caption != null ? escapeString(frame.Caption) : "", ctl.Height * 20, ctl.Left * 20, count, ctl.Top * 20, ctl.Width * 20, frame.BackColor.ToString("X2"), frame.ForeColor.ToString("X2"), getFontString(frame.Font), identedFrameChildren);

                strControlCode += frameTemplate;
            }


            // if (ctl is Outlook.OlkTextBox) 
            // if (ctl is Microsoft.Vbe.Interop.Forms.TextBox)
            if (ctl is Microsoft.Vbe.Interop.Forms.TextBox || ctl is Microsoft.Vbe.Interop.Forms.TextBox)
            {
                // Outlook.OlkTextBox textBox = (Outlook.OlkTextBox)ctl;
                // Microsoft.Vbe.Interop.Forms.TextBox textBox = (Microsoft.Vbe.Interop.Forms.TextBox)ctl;  
                dynamic textBox = (dynamic)ctl;  
                
                string textboxTemplate = String.Format(@"  
    Begin VB.TextBox {0}
        Height          =   {1}
        Left            =   {2}
        TabIndex        =   {3}
        Text            =   ""{4}""
        Top             =   {5}
        Width           =   {6}
        BackColor       =   &H{7}&
        ForeColor       =   &H{8}&
        Alignment       =   {9}
        {10}
    End",
                ctl.Name.Replace(" ", ""), ctl.Height * 20, ctl.Left * 20, count, textBox.Text != null ? escapeString(textBox.Text) : "", ctl.Top * 20, ctl.Width * 20, textBox.BackColor.ToString("X2"), textBox.ForeColor.ToString("X2"), toVBTextAlign((int)textBox.TextAlign), getFontString(textBox.Font));

                strControlCode += textboxTemplate;
            }


            if (ctl is Outlook.OlkCommandButton)
            // if (ctl is Microsoft.Vbe.Interop.Forms.CommandButton)
            // if (ctl is Outlook.OlkCommandButton || ctl is Microsoft.Vbe.Interop.Forms.CommandButton)
            {
                Microsoft.Vbe.Interop.Forms.CommandButton cmdButton = (Microsoft.Vbe.Interop.Forms.CommandButton)ctl;
                // Outlook.OlkCommandButton cmdButton = (Outlook.OlkCommandButton)ctl;
                // dynamic cmdButton = (dynamic)ctl;

                string buttonTemplate = String.Format(@"
    Begin VB.CommandButton {0}
        Caption         =   ""{1}""
        Height          =   {2}
        Left            =   {3}
        TabIndex        =   {4}
        Top             =   {5}
        Width           =   {6}
        {7}
    End",
                    ctl.Name.Replace(" ", ""), escapeString(cmdButton.Caption), (int)(ctl.Height * 20), (int)(ctl.Left * 20), count, (int)(ctl.Top * 20),
                    (int)(ctl.Width * 20),  getFontString(cmdButton.Font));

                strControlCode += buttonTemplate;
            }

            // if (ctl is Outlook.OlkLabel) 
            // if (ctl is Microsoft.Vbe.Interop.Forms.Label)
            if (ctl is Outlook.OlkLabel || ctl is Microsoft.Vbe.Interop.Forms.Label)
            {
                // Outlook.OlkLabel label = (Outlook.OlkLabel)ctl;
                // Microsoft.Vbe.Interop.Forms.Label label = (Microsoft.Vbe.Interop.Forms.Label)ctl;
                dynamic label = (dynamic)ctl;

                string labelTemplate = String.Format(@"
   Begin VB.Label {0} 
      Caption         =   ""{1}""
      Height          =   {2}
      Left            =   {3}
      TabIndex        =   {4}
      Top             =   {5}
      Width           =   {6}
      BackColor       =   &H{7}&
      ForeColor       =   &H{8}&
      {9}
   End",
                    ctl.Name.Replace(" ", ""), label.Caption != null ? escapeString(label.Caption) : "", (int)(ctl.Height * 20), (int)(ctl.Left * 20),
                    count, (int)(ctl.Top * 20), (int)(ctl.Width * 20), label.BackColor.ToString("X2"), label.ForeColor.ToString("X2"), getFontString(label.Font));

                strControlCode += labelTemplate;
            }

            if (ctl is Image)
            {
                Image img = (Image)ctl;

                string imageTemplate = String.Format(@"
   Begin VB.PictureBox {0} 
      Height          =   {1}
      Left            =   {2}
      ScaleHeight     =   {3}
      ScaleWidth      =   {4}
      TabIndex        =   {5}
      Top             =   {6}
      Width           =   {7}
      BackColor       =   &H{7}&
   End",
                    ctl.Name.Replace(" ", ""), (int)(ctl.Height * 20), (int)(ctl.Left * 20), (int)(ctl.Height * 20 * 0.91), (int)(ctl.Left * 20 * 0.91),
                    count, (int)(ctl.Top * 20), (int)(ctl.Width * 20), img.BackColor.ToString("X2"));

                strControlCode += imageTemplate;
            }

            if (ctl is Microsoft.Vbe.Interop.Forms.OptionButton)
            // if (ctl is Outlook.OlkOptionButton || ctl is Microsoft.Vbe.Interop.Forms.OptionButton)
            {
                /*
                dynamic optButton;
                if (ctl is Outlook.OlkOptionButton)
                    optButton = (Outlook.OlkOptionButton)ctl;
                else
                    optButton = (Microsoft.Vbe.Interop.Forms.OptionButton)ctl;
                */
                Microsoft.Vbe.Interop.Forms.OptionButton optButton = (Microsoft.Vbe.Interop.Forms.OptionButton)ctl;

                string optBtnTemplate = String.Format(@"  
    Begin VB.OptionButton {0} 
        Caption         =   ""{1}""
        Height          =   {2}
        Left            =   {3}
        TabIndex        =   {4}
        Top             =   {5}
        Width           =   {6}
        BackColor       =   &H{7}&
        ForeColor       =   &H{8}&
        {9}
    End",

                     ctl.Name.Replace(" ", ""), escapeString(optButton.Caption), (int)(ctl.Height * 20), (int)(ctl.Left * 20), count, (int)(ctl.Top * 20), (int)(ctl.Width * 20), optButton.BackColor.ToString("X2"), optButton.ForeColor.ToString("X2"), getFontString(optButton.Font));

                strControlCode += optBtnTemplate;
            }

            if (ctl is Outlook.OlkComboBox || ctl is Microsoft.Vbe.Interop.Forms.ComboBox)
            {
                dynamic comboBtn;
                if (ctl is Outlook.OlkComboBox)
                    comboBtn = (Outlook.OlkComboBox)ctl;
                else
                    comboBtn = (Microsoft.Vbe.Interop.Forms.ComboBox)ctl;
                
                int cbbStyle = 0;
                string comboTemplate = String.Format(@"  
    Begin VB.ComboBox {0} 
        Height          =   {1}
        Left            =   {2}
        Style           =   {3}
        TabIndex        =   {4}
        Top             =   {5}
        Width           =   {6}
        BackColor       =   &H{7}&
        ForeColor       =   &H{8}&
        {9}
   End", ctl.Name.Replace(" ", ""), (int)(ctl.Height * 20), (int)(ctl.Left * 20), cbbStyle, count, (int)(ctl.Top * 20), (int)(ctl.Width * 20), comboBtn.BackColor.ToString("X2"), comboBtn.ForeColor.ToString("X2"), getFontString(comboBtn.Font));

                strControlCode += comboTemplate;
            }
        }

        public static void exportFormElements2(ImportedFormRegionBase form)
        {
            UserForm oForm = form.OutlookFormRegion.Form;

            addedControls.Clear();

            int count = 0;
            string strControlCode = "";

            foreach (Control ctl in oForm.Controls)
            {
                exportFromRoot(ctl, ref count, ref strControlCode);
            }

            TextWriter tw = new StreamWriter("Z:\\forms\\" + form.GetType().Name + ".frm", false);
            tw.WriteLine(String.Format(Resources.FormTemplate, (int)(oForm.InsideHeight*20), (int)(oForm.InsideWidth*20), (int)(oForm.InsideHeight *20/ 2.5), (int)(oForm.InsideWidth *20/ 2.5),
                        oForm.BackColor.ToString("X2"), oForm.ForeColor.ToString("X2"), strControlCode, form.GetType().Name));
            tw.Close();
        }
    }
}
