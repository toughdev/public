namespace RadioDeviceStatus
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.mnuRefresh = new System.Windows.Forms.MenuItem();
            this.chkPhone = new System.Windows.Forms.CheckBox();
            this.chkWifi = new System.Windows.Forms.CheckBox();
            this.chkBluetooth = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.mnuRefresh);
            // 
            // mnuRefresh
            // 
            this.mnuRefresh.Text = "Refresh";
            this.mnuRefresh.Click += new System.EventHandler(this.mnuRefresh_Click);
            // 
            // chkPhone
            // 
            this.chkPhone.Location = new System.Drawing.Point(3, 56);
            this.chkPhone.Name = "chkPhone";
            this.chkPhone.Size = new System.Drawing.Size(100, 20);
            this.chkPhone.TabIndex = 0;
            this.chkPhone.Text = "Phone";
            this.chkPhone.CheckStateChanged += new System.EventHandler(this.chkPhone_CheckStateChanged);
            // 
            // chkWifi
            // 
            this.chkWifi.Location = new System.Drawing.Point(3, 82);
            this.chkWifi.Name = "chkWifi";
            this.chkWifi.Size = new System.Drawing.Size(100, 20);
            this.chkWifi.TabIndex = 1;
            this.chkWifi.Text = "Wifi";
            this.chkWifi.CheckStateChanged += new System.EventHandler(this.chkWifi_CheckStateChanged);
            // 
            // chkBluetooth
            // 
            this.chkBluetooth.Location = new System.Drawing.Point(3, 108);
            this.chkBluetooth.Name = "chkBluetooth";
            this.chkBluetooth.Size = new System.Drawing.Size(100, 20);
            this.chkBluetooth.TabIndex = 2;
            this.chkBluetooth.Text = "Bluetooth";
            this.chkBluetooth.CheckStateChanged += new System.EventHandler(this.chkBluetooth_CheckStateChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(233, 20);
            this.label1.Text = "Check or uncheck to change status.";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkBluetooth);
            this.Controls.Add(this.chkWifi);
            this.Controls.Add(this.chkPhone);
            this.Menu = this.mainMenu1;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Radio Device Status";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Form1_Closing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkPhone;
        private System.Windows.Forms.CheckBox chkWifi;
        private System.Windows.Forms.CheckBox chkBluetooth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuItem mnuRefresh;

    }
}

