using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace RadioDeviceStatus
{
    public class RadioDevice
    {
        [MarshalAs(UnmanagedType.LPTStr)]
        public string pszDeviceName;

        [MarshalAs(UnmanagedType.LPTStr)]
        public string pszDisplayName;

        public RADIODEVSTATE dwState;
        public int dwDesired;
        public RADIODEVTYPE DeviceType;

        public IntPtr pNext;

        public RadioDevice()
        {
            pszDeviceName = null;
            pszDisplayName = null;
            pNext = IntPtr.Zero;
        }

        ~RadioDevice()
        {
        }
    }
}
