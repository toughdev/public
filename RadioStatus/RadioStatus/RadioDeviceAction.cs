using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace RadioDeviceStatus
{
    public enum RADIODEVTYPE : int
    {
        POWER_MANAGED = 1,
        POWER_PHONE = 2,
        POWER_BLUETOOTH = 3
    }

    public enum RADIODEVSTATE : int
    {
        DEVICE_OFF = 0,
        DEVICE_ON = 1,
        DEVICE_BLUETOOTH_DISCOVERABLE = 2
    }

    public enum SAVEACTION : int
    {
        POWER_DONT_SAVE = 0,
        POWER_PRE_SAVE = 1,
        POWER_POST_SAVE = 2
    }

    public static class RadioDeviceAction
    {
        public static RADIODEVSTATE GetDeviceStatus(RADIODEVTYPE deviceType)
        {
            //HRESULT
            int hr;

            //get the list of wireless devices available
            RadioDevice FirstDevice = new RadioDevice();
            hr = RadioDeviceAction.GetWirelessDevices(ref FirstDevice, 0);
            if (hr < 0)
                throw new NotSupportedException("GetWirelessDevices FAILED! HR=" + hr.ToString());

            //if success, traverse through list and locate each device type
            RadioDevice thisDevice = FirstDevice;
            bool noMoreDevices = false;
            while (!noMoreDevices)
            {
                //found requested device, return status
                if (thisDevice.DeviceType == deviceType)
                {
                    return thisDevice.dwState;
                }

                if (thisDevice.pNext != IntPtr.Zero)
                    Marshal.PtrToStructure(thisDevice.pNext, thisDevice);
                else
                    noMoreDevices = true;
            }

            //if we are here, device cannot be found
            throw new ArgumentException("Device cannot be found");

        }

        public static void ChangeDeviceStatus(RADIODEVTYPE deviceType, RADIODEVSTATE deviceState)
        {
            //HRESULT
            int hr;

            //get the list of wireless devices available
            RadioDevice FirstDevice = new RadioDevice();
            hr = RadioDeviceAction.GetWirelessDevices(ref FirstDevice, 0);
            if (hr < 0)
                throw new NotSupportedException("GetWirelessDevices FAILED! HR=" + hr.ToString());

            //if success, traverse through list and locate each device type
            RadioDevice thisDevice = FirstDevice;
            bool noMoreDevices = false;
            while (!noMoreDevices)
            {
                //found requested device, change status
                if (thisDevice.DeviceType == deviceType)
                {
                    hr = RadioDeviceAction.ChangeRadioState(thisDevice, deviceState, SAVEACTION.POWER_PRE_SAVE);
                    if (hr < 0)
                        throw new NotSupportedException("ChangeRadioState FAILED! HR=" + hr.ToString());

                    //done, no more searching
                    return;
                }

                if (thisDevice.pNext != IntPtr.Zero)
                    Marshal.PtrToStructure(thisDevice.pNext, thisDevice);
                else
                    noMoreDevices = true;
            }

            //if we are here, device cannot be found
            throw new ArgumentException("Device cannot be found");
        }

        [DllImport("ossvcs.dll", EntryPoint = "#276")]
        private static extern int GetWirelessDevices(ref RadioDevice RDD, int dwFlags);

        [DllImport("ossvcs.dll", EntryPoint = "#273")]
        private static extern int ChangeRadioState(RadioDevice RDD, RADIODEVSTATE dwFlags, SAVEACTION action);
    }
}
