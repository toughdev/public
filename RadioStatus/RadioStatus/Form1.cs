using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;

namespace RadioDeviceStatus
{
    public partial class Form1 : Form
    {
        //Checked events of checkbox may fire randomly on startup. 
        //Use this to "lock" them until form has completely loaded.
        bool isOK = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GetStatus();
        }

        private void mnuRefresh_Click(object sender, EventArgs e)
        {
            GetStatus();
        }

        private void GetStatus()
        {
            RADIODEVSTATE bluetoothStatus = RadioDeviceAction.GetDeviceStatus(RADIODEVTYPE.POWER_BLUETOOTH);
            RADIODEVSTATE phoneStatus = RadioDeviceAction.GetDeviceStatus(RADIODEVTYPE.POWER_PHONE);
            RADIODEVSTATE wifiStatus = RadioDeviceAction.GetDeviceStatus(RADIODEVTYPE.POWER_MANAGED);

            this.chkBluetooth.Checked = (bluetoothStatus == RADIODEVSTATE.DEVICE_ON) || (bluetoothStatus == RADIODEVSTATE.DEVICE_BLUETOOTH_DISCOVERABLE);
            this.chkPhone.Checked = (phoneStatus == RADIODEVSTATE.DEVICE_ON);
            this.chkWifi.Checked = (wifiStatus == RADIODEVSTATE.DEVICE_ON);

            isOK = true;
        }

        private void chkPhone_CheckStateChanged(object sender, EventArgs e)
        {
            if (!isOK) return;

            if (this.chkPhone.Checked)
                RadioDeviceAction.ChangeDeviceStatus(RADIODEVTYPE.POWER_PHONE, RADIODEVSTATE.DEVICE_ON);
            else
                RadioDeviceAction.ChangeDeviceStatus(RADIODEVTYPE.POWER_PHONE, RADIODEVSTATE.DEVICE_OFF);
        }

        private void chkWifi_CheckStateChanged(object sender, EventArgs e)
        {
            if (!isOK) return;

            if (this.chkWifi.Checked)
                RadioDeviceAction.ChangeDeviceStatus(RADIODEVTYPE.POWER_MANAGED, RADIODEVSTATE.DEVICE_ON);
            else
                RadioDeviceAction.ChangeDeviceStatus(RADIODEVTYPE.POWER_MANAGED, RADIODEVSTATE.DEVICE_OFF);

        }

        private void chkBluetooth_CheckStateChanged(object sender, EventArgs e)
        {
            if (!isOK) return;

            if (this.chkBluetooth.Checked)
                RadioDeviceAction.ChangeDeviceStatus(RADIODEVTYPE.POWER_BLUETOOTH, RADIODEVSTATE.DEVICE_ON);
            else
                RadioDeviceAction.ChangeDeviceStatus(RADIODEVTYPE.POWER_BLUETOOTH, RADIODEVSTATE.DEVICE_OFF);

        }

        private void Form1_Closing(object sender, CancelEventArgs e)
        {
            isOK = false;
        }
    }
}