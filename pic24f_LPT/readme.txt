Code of a hobbyist project in which I used a PIC to emulate a parallel port printer and store all the captured data from a Tektronix 1230 logic analyzer to an SD card, for viewing on a desktop computer. 

See details at http://www.toughdev.com/2014/02/capturing-data-from-tektronix-1230.html