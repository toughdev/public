#include <p24fj64ga002.h>
#include "delay.h"

//function iniates a start condition on bus
void start_i2c(void)
{
   int x = 0;
   I2C2CONbits.ACKDT = 0;	//Reset any previous Ack
   delay_us(10);
   I2C2CONbits.SEN = 1;	//Initiate Start condition
   Nop();

   //the hardware will automatically clear Start Bit
   //wait for automatic clear before proceding
   while (I2C2CONbits.SEN)
   {
      delay_us(1);
      x++;
      if (x > 20)
      break;
   }
   delay_us(2);
}

void restart_i2c(void)
{
   int x = 0;

   I2C2CONbits.RSEN = 1;	//Initiate restart condition
   Nop();
    
   //the hardware will automatically clear restart bit
   //wait for automatic clear before proceding
   while (I2C2CONbits.RSEN)
   {
      delay_us(1);
      x++;
      if (x > 20)	break;
   }
    
   delay_us(2);
}

void reset_i2c_bus(void)
{
   int x = 0;

   //initiate stop bit
   I2C2CONbits.PEN = 1;

   //wait for hardware clear of stop bit
   while (I2C2CONbits.PEN)
   {
      delay_us(1);
      x ++;
      if (x > 20) break;
   }
   I2C2CONbits.RCEN = 0;
   IFS3bits.MI2C2IF = 0; // Clear Interrupt
   I2C2STATbits.IWCOL = 0;
   I2C2STATbits.BCL = 0;
   delay_us(10);
}

//function initiates I2C1 module to baud rate BRG
void init_i2c(int BRG)
{
   int temp;

   // I2CBRG = 194 for 10Mhz OSCI with PPL with 100kHz I2C clock
   I2C2BRG = BRG;
   I2C2CONbits.I2CEN = 0;	// Disable I2C Mode
   I2C2CONbits.DISSLW = 1;	// Disable slew rate control
   IFS3bits.MI2C2IF = 0;	 // Clear Interrupt
   I2C2CONbits.I2CEN = 1;	// Enable I2C Mode
   temp = I2C2RCV;	 // read buffer to clear buffer full
   reset_i2c_bus();	 // set bus to idle
}

//basic I2C byte send
char send_i2c_byte(char data)
{
   int i;

   while (I2C2STATbits.TBF) { }

   IFS3bits.MI2C2IF = 0; // Clear Interrupt
   I2C2TRN = data; // load the outgoing data byte

   // wait for transmission
   for (i=0; i<500; i++)
   {
      if (!I2C2STATbits.TRSTAT) break;
      delay_us(1);

      }
      if (i == 500) {
      return(1);
   }

   // Check for NO_ACK from slave, abort if not found
   if (I2C2STATbits.ACKSTAT == 1)
   {
      reset_i2c_bus();
      return(1);
   }
   
   delay_us(2);
   return(0);
}

//function reads data, returns the read data, no ack
char read_i2c(void)
{
   int i = 0;
   char data = 0;

   //set I2C module to receive
   I2C2CONbits.RCEN = 1;

   //if no response, break
   while (!I2C2STATbits.RBF)
   {
      i ++;
      if (i > 2000) break;
   }

   //get data from I2CRCV register
   data = I2C2RCV;

   //return data
   return data;
}

//function reads data, returns the read data, with ack
char read_ack_i2c(void)	//does not reset bus!!!
{
   int i = 0;
   char data = 0;

   //set I2C module to receive
   I2C2CONbits.RCEN = 1;

   //if no response, break
   while (!I2C2STATbits.RBF)
   {
      i++;
      if (i > 2000) break;
   }

   //get data from I2CRCV register
   data = I2C2RCV;

   //set ACK to high
   I2C2CONbits.ACKEN = 1;

   //wait before exiting
   delay_us(10);

   //return data
   return data;
}
