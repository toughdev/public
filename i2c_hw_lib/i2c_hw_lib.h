// Hardware I2C library on a PIC24FJ64GA002
// Things to note:
// 1. Some PIC24F has silicon bug which prevents I2C1 to work. Use I2C2 instead
// 2. If code is not working, use a 100pF capacitor to connect SDA line to ground.
// References:
// http://www.engscope.com/pic24-tutorial/10-2-i2c-basic-functions/
// http://www.microchip.com.edgekey.net/forums/m647015-print.aspx

void start_i2c(void);
void restart_i2c(void);
void reset_i2c_bus(void);
void init_i2c(int BRG);
char send_i2c_byte(char data);
char read_i2c(void);
char read_ack_i2c(void);
