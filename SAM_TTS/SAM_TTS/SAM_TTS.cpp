// Text to Speech on a PC.
// Adapted from http://simulationcorner.net/index.php?page=sam
// SAM Software Automatic Mouth
//

#include "stdafx.h"
#include "string.h"
#include <stdlib.h>
#include <stdio.h>
#include "sam.h"
#include "ctype.h"
#include "reciter.h"
#include "windows.h"

void WriteWav(char* filename, char* buffer, INT32 bufferlength)
{
	FILE *file = fopen(filename, "wb");
	if (file == NULL) return;
	//RIFF header
	fwrite("RIFF", 4, 1,file);
	
	// original:
	// unsigned INT32 filesize=bufferlength + 12 + 16 + 8 - 8;

	// file size, corrected by MD
	// see: https://ccrma.stanford.edu/courses/422/projects/WaveFormat/
	UINT32 filesize=bufferlength + 36;

	fwrite(&filesize, 4, 1, file);
	fwrite("WAVE", 4, 1, file);

	//format chunk
	fwrite("fmt ", 4, 1, file);
	UINT32 fmtlength = 16;
	fwrite(&fmtlength, 4, 1, file);
	UINT16 format=1; //PCM
	fwrite(&format, 2, 1, file);
	UINT16 channels=1;
	fwrite(&channels, 2, 1, file);
	UINT32 samplerate = 22050;
	fwrite(&samplerate, 4, 1, file);
	fwrite(&samplerate, 4, 1, file); // bytes/second
	UINT16 blockalign = 1;
	fwrite(&blockalign, 2, 1, file);
	UINT16 bitspersample=8;
	fwrite(&bitspersample, 2, 1, file);

	//data chunk
	fwrite("data", 4, 1, file);
	fwrite(&bufferlength, 4, 1, file);
	fwrite(buffer, bufferlength, 1, file);

	fclose(file);
}

void printUsage()
{
	// -wav c:\temp\i_am_sam.wav This is Sam Text to Speech speaking to you.

	printf("usage: sam [options] Word1 Word2 ....\n");
	printf("options\n");
	printf("	-phonetic 		enters phonetic mode. (see below)\n");
	printf("	-pitch number		set pitch value (default=64)\n");
	printf("	-speed number		set speed value (default=72)\n");
	printf("	-throat number		set throat value (default=128)\n");
	printf("	-mouth number		set mouth value (default=128)\n");
	printf("	-wav filename		output to wav instead of libsdl\n");
	printf("	-sing			special treatment of pitch\n");
	printf("	-debug			print additional debug messages\n");
	printf("\n");


	printf("     VOWELS                            VOICED CONSONANTS	\n");
	printf("IY           f(ee)t                    R        red		\n");
	printf("IH           p(i)n                     L        allow		\n");
	printf("EH           beg                       W        away		\n");
	printf("AE           Sam                       W        whale		\n");
	printf("AA           pot                       Y        you		\n");
	printf("AH           b(u)dget                  M        Sam		\n");
	printf("AO           t(al)k                    N        man		\n");
	printf("OH           cone                      NX       so(ng)		\n");
	printf("UH           book                      B        bad		\n");
	printf("UX           l(oo)t                    D        dog		\n");
	printf("ER           bird                      G        again		\n");
	printf("AX           gall(o)n                  J        judge		\n");
	printf("IX           dig(i)t                   Z        zoo		\n");
	printf("				       ZH       plea(s)ure	\n");
	printf("   DIPHTHONGS                          V        seven		\n");
	printf("EY           m(a)de                    DH       (th)en		\n");
	printf("AY           h(igh)						\n");
	printf("OY           boy						\n");
	printf("AW           h(ow)                     UNVOICED CONSONANTS	\n");
	printf("OW           slow                      S         Sam		\n");
	printf("UW           crew                      Sh        fish		\n");
	printf("                                       F         fish		\n");
	printf("                                       TH        thin		\n");
	printf(" SPECIAL PHONEMES                      P         poke		\n");
	printf("UL           sett(le) (=AXL)           T         talk		\n");
	printf("UM           astron(omy) (=AXM)        K         cake		\n");
	printf("UN           functi(on) (=AXN)         CH        speech		\n");
	printf("Q            kitt-en (glottal stop)    /H        a(h)ead	\n");	
}

INT32 main(INT32 argc, char **argv)
{
	INT32 i;
	INT32 debug = 1;
	INT32 phonetic = 0;
	INT32 wavfilenameposition = -1;
	char input[256];

	for(i=0; i<256; i++) input[i] = 0;

	if (argc <= 1)
	{
		printUsage();
		return 1;
	}

	input[0]=0;
	strcat(input, " ");

	i = 1;
	while(i < argc)
	{
		if (argv[i][0] != '-')
		{
			strcat(input, argv[i]);
			strcat(input, " ");
		} else
		{
			if (strcmp(&argv[i][1], "wav")==0)
			{
				wavfilenameposition = i+1;
				i++;
			} else
				if (strcmp(&argv[i][1], "sing")==0)
				{
					EnableSingmode(1);
				} else
					if (strcmp(&argv[i][1], "phonetic")==0)
					{
						phonetic = 1;
					} else
						if (strcmp(&argv[i][1], "debug")==0)
						{
							debug = 1;
						} else
							if (strcmp(&argv[i][1], "pitch")==0)
							{
								SetPitch(atoi(argv[i+1]));
								i++;
							} else
								if (strcmp(&argv[i][1], "speed")==0)
								{
									SetSpeed(atoi(argv[i+1]));
									i++;
								} else
									if (strcmp(&argv[i][1], "mouth")==0)
									{
										SetMouth(atoi(argv[i+1]));
										i++;
									} else
										if (strcmp(&argv[i][1], "throat")==0)
										{
											SetThroat(atoi(argv[i+1]));
											i++;
										} else
										{
											printUsage();
											return 1;
										}
		}

		i++;
	} //while

	strcat(input, " ");
	for(i=0; input[i] != 0; i++)
		input[i] = toupper(input[i]);

	if (debug)
	{
		printf("say: %s\n", input);
	}

	if (!phonetic)
	{
		if (!TextToPhonemes(input)) return 1;
		if (debug)
			printf("text translation: STARTS %s END\n", input);
	}
	strcat(input, " \x9b\0");

	SetInput(input);
	if (!Code39771())
	{
		printUsage();
		return 1;
	}

	if (wavfilenameposition > 0) 
		WriteWav(argv[wavfilenameposition], GetBuffer(), GetBufferLength()/50);

	if (debug) PrintDebug();	

	getc(stdin);

	return 0;

}
