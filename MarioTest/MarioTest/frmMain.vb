﻿Public Class frmMain

    Private Sub btnSpriteView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpriteView.Click
        frmSpriteViewer.ShowDialog()
    End Sub

    Private Sub btnLevelView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLevelView.Click
        frmLevelViewer.ShowDialog()
    End Sub
End Class