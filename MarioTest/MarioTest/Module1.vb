﻿Imports System.Xml.Serialization
Imports System.IO

Public Module Module1

    'Each block on the screen is represented by one character in level data
    'The size of the screen is therefore (Width = W*NH, Height = H*NV)

    Public Const SpriteWidth As Integer = 20  'original: W, width of each sprite
    Public Const SpriteHeight As Integer = 14  'original: H, height of each sprivate

    ''' <summary>
    ''' Original: NV, number of vertical blocks per screen
    ''' </summary>
    Public Const ScreenWidthInBlks As Integer = 13

    ''' <summary>
    ''' Original: NH, number of horizontal blocks per screen
    ''' </summary>
    Public Const ScreenHeightInBlks As Integer = 16

    Public Const XView As Integer = 0
    Public Const EX As Integer = 1
    Public Const EY1 As Integer = 8
    Public Const EY2 As Integer = 3
    Public Const N1 As Integer = 3
    Public Const N2 As Integer = 13
    Const XSize As Integer = 18

    Const MaxWorldSize As Integer = 236
    Const CanHoldYou As Char = Chr(0)

    Const LavaCounter As Integer = 0 'to switch between different frames and simulate motion of water in a LAVA

#Region "Read Palette & Sprites"
    Private PaletteFileName As String = IO.Path.GetDirectoryName(Application.ExecutablePath) + "\DEFAULT.PAL"
    Private Const MAX_PALETTE_COLORS As Integer = 256
    Private Color_Palette(0 To MAX_PALETTE_COLORS - 1) As Color

    Private HasReadPalette As Boolean = False

    ''' <summary>
    ''' Read the color palette stored in DEFAULT.PAL into the array VGA_Palette
    ''' </summary>
    Private Sub ReadPalette()
        Dim palBytes = My.Computer.FileSystem.ReadAllBytes(PaletteFileName)
        If palBytes.Length <> (MAX_PALETTE_COLORS * 3) Then
            Throw New FormatException("Invalid color pallette file")
        Else
            'palBytes(i): color stored in palette file has RGB values from 0..63
            '.NET Color structure expect RGB values from 0..255
            For i As Integer = 0 To MAX_PALETTE_COLORS - 1
                Dim valRed = (palBytes(i * 3) + 1) * 4 - 1
                Dim valGreen = (palBytes(i * 3 + 1) + 1) * 4 - 1
                Dim valBlue = (palBytes(i * 3 + 2) + 1) * 4 - 1

                Dim thisColor As Color = Color.FromArgb(valRed, valGreen, valBlue)
                Color_Palette(i) = thisColor
            Next
        End If
    End Sub

    ''' <summary>
    ''' Convert a sprite (.000, .001, etc) created using GRED.EXE to a Bitmap.
    ''' </summary>
    Public Function ConvertSpriteToBitmap(ByVal filename As String) As Bitmap
        ReadPalette()

        Dim picBytes = My.Computer.FileSystem.ReadAllBytes(filename)
        Dim height = picBytes(1)
        Dim width = picBytes(0)

        Dim bmp As New Bitmap(width, height)
        For i As Integer = 0 To height - 1
            For j As Integer = 0 To width - 1
                Dim k = i * width + j + 2

                'do not paint transparent color (code 0)
                If picBytes(k) <> 0 Then
                    Dim thisPixel As Color = Color_Palette(picBytes(k))
                    bmp.SetPixel(j, i, thisPixel)
                End If
            Next
        Next

        Return bmp
    End Function
#End Region

#Region "Read Level Data"
    Private Const WorldBufferWidth As Integer = MaxWorldSize + 2 * EX
    Private Const WorldBufferHeight As Integer = ScreenWidthInBlks + EY2 + EY1

    ''' <summary>
    ''' The properties of objects in each level. This is originally stored in the 
    ''' assembler procedure Options_XX, Opt_XX in WORLDS.PAS.
    ''' </summary>
    Public Structure LevelProperties
        Dim InitX As Integer
        Dim InitY As Integer
        Dim Walls1 As Integer
        Dim Walls2 As Integer
        Dim Walls3 As Integer
        Dim Pipes As Integer
        Dim GroundColor1 As Integer
        Dim GroundColor2 As Integer
        Dim Horizon As Integer
        Dim BackGrType As Integer
        Dim BackGrColor1 As Integer
        Dim BackGrColor2 As Integer
        Dim Stars As Integer
        Dim Clouds As Integer
        Dim Design As Integer
        Dim Color2 As Integer
        Dim Color3 As Integer
        Dim BrickColor As Integer
        Dim WoodColor As Integer
        Dim XBlockColor As Integer
    End Structure

    'original Pascal declaration
    'var WorldBuffer = array [-EX .. MaxWorldSize - 1 + EX, -EY1 .. NV - 1 + EY2] of Char;
    'As VB.NET does not support non-zero array base, a property WorldMap is used
    'to avoid extensive modifications to the original Pascal source code.
    Private WorldBuffer(0 To WorldBufferWidth - 1, 0 To WorldBufferHeight - 1) As Char

    Private Property WorldMap(ByVal X As Integer, ByVal Y As Integer) As Char
        Get
            Return WorldBuffer(X + EX, Y + EY1)
        End Get
        Set(ByVal value As Char)
            WorldBuffer(X + EX, Y + EY1) = value
        End Set
    End Property

    ''' <summary>
    ''' Read the level binary data stored in the specified file into a list
    ''' </summary>
    Public Function ReadLevelData(ByVal filename As String) As List(Of String)
        Dim output As New List(Of String)

        Dim lvlRawData As Byte() = My.Computer.FileSystem.ReadAllBytes(filename)

        Dim curLine As String = String.Empty
        For i As Integer = 0 To lvlRawData.Length - 1
            curLine += Chr(lvlRawData(i))

            If (i + 1) Mod ScreenWidthInBlks = 0 Then
                output.Add(curLine)
                curLine = String.Empty
            End If
        Next

        Return output
    End Function

    ''' <summary>
    ''' Read the level properties (usually stored in an XML file having the same name).
    ''' Return TRUE if successful
    ''' </summary>
    ''' <param name="filename">Path to the XML file name</param>
    ''' <param name="lvlProp">The properties that were read</param>
    Public Function ReadLevelProperty(ByVal filename As String, ByRef lvlProp As LevelProperties) As Boolean
        Dim isok As Boolean = False
        Dim outObj = readSerializedData(filename, lvlProp.GetType, isok)
        If isok Then lvlProp = CType(outObj, LevelProperties)
        Return isok
    End Function
#End Region

#Region "Draw Level Image"
    'the final screen for the current stage of the level.
    Public CurrentLevelScreen As Bitmap

    'transparency color code. Any pixel in the bitmap having this color will not be drawn
    Private TransparencyColor As Color = Color.FromArgb(0, 0, 0, 0)

    ''' <summary>
    ''' Change the color scheme of the specified bitmap and return the new bitmap.
    ''' </summary>
    ''' <param name="fig">The original bitmap</param>
    ''' <remarks>
    ''' Original: procedure ReColor (P1, P2: Pointer; C: Byte); (FIGURES.PAS)
    ''' </remarks>
    Private Function ReColor(ByVal fig As Bitmap, ByVal factor As Integer) As Bitmap
        If Not HasReadPalette Then
            HasReadPalette = True
            ReadPalette()
        End If

        For i As Integer = 0 To fig.Width - 1
            For j As Integer = 0 To fig.Height - 1
                Dim thisPixel = fig.GetPixel(i, j)
                If thisPixel <> Color_Palette(16) And thisPixel <> TransparencyColor Then
                    'search the original color code if the pixel in the palette
                    Dim originalColorCode As Integer = -1
                    For ind As Integer = 0 To Color_Palette.Length - 1
                        If Color_Palette(ind) = thisPixel Then
                            originalColorCode = ind
                            Exit For
                        End If
                    Next

                    If originalColorCode <> -1 Then 'found
                        'TODO: New image looks alright but does not match the exact color in the original game!
                        Dim newColorCode As Integer = originalColorCode And &H7 + factor
                        Dim newColor As Color = Color_Palette(newColorCode)

                        fig.SetPixel(i, j, newColor)
                    End If
                End If
            Next
        Next

        Return fig
    End Function

    ''' <summary>
    ''' Change the color scheme of the specified bitmap and return the new bitmap. 
    ''' </summary>
    ''' <param name="fig">The original bitmap</param>
    ''' <remarks>
    ''' Original: procedure ReColor2 (P1, P2: Pointer; C1, C2: Byte); (FIGURES.PAS)
    ''' </remarks>
    Private Function ReColor2(ByVal fig As Bitmap, ByVal factor1 As Integer, ByVal factor2 As Integer) As Bitmap
        If Not HasReadPalette Then
            HasReadPalette = True
            ReadPalette()
        End If

        For i As Integer = 0 To fig.Width - 1
            For j As Integer = 0 To fig.Height - 1
                Dim thisPixel = fig.GetPixel(i, j)
                If thisPixel <> Color_Palette(16) And thisPixel <> TransparencyColor Then
                    'search the original color code if the pixel in the palette
                    Dim originalColorCode As Integer = -1
                    For ind As Integer = 0 To Color_Palette.Length - 1
                        If Color_Palette(ind) = thisPixel Then
                            originalColorCode = ind
                            Exit For
                        End If
                    Next

                    If originalColorCode <> -1 Then 'found
                        Dim newColorCode As Integer = originalColorCode And &HF

                        If newColorCode < 8 Then
                            newColorCode = newColorCode + factor1
                        Else
                            newColorCode = newColorCode And &H7 + factor2
                        End If

                        Dim newColor As Color = Color_Palette(newColorCode)
                        fig.SetPixel(i, j, newColor)
                    End If
                End If
            Next
        Next

        Return fig
    End Function

    'from procedure BuildWorld; (FIGURES.PAS)
    Dim AB As Char
    Dim CD As Char
    Dim EF As Char
    Dim LastAB As Char
    Dim LastCD As Char
    Dim LastEF As Char

    ''' <summary>
    ''' Original:
    ''' procedure BuildWall (X, Y: Integer); (FIGURES.PAS)
    ''' </summary>
    Private Sub BuildWall(ByVal X As Integer, ByVal Y As Integer)
        Dim IgnoreAbove As Char = "÷"c
        Dim A As Integer, B As Integer, L As Integer, R As Integer, N As Integer
        Dim C As Char

        'original
        'Ch, ChLeft: Set of Char;
        Dim Ch As New List(Of Char), ChLeft As New List(Of Char)

        C = WorldMap(X, Y)

        'Debug.WriteLine(String.Format("BuildWall(X = {0}, Y = {1}, OrgCh = {2})", X, Y, Asc(C)))

        Select Case C
            Case "A"c, "B"c
                AB = C

                'original
                'Ch = [C] + [#1 .. #13]
                Ch.Add(C)
                For i As Integer = 1 To 13
                    If Not Ch.Contains(Chr(i)) Then Ch.Add(Chr(i))
                Next

                If LastAB <> C Then
                    'original
                    'ChLeft = Ch - [#3, #6, #9]
                    Dim tempCh As New List(Of Char)
                    tempCh.AddRange(Ch)

                    If tempCh.Contains(Chr(3)) Then tempCh.RemoveAt(tempCh.IndexOf(Chr(3)))
                    If tempCh.Contains(Chr(6)) Then tempCh.RemoveAt(tempCh.IndexOf(Chr(6)))
                    If tempCh.Contains(Chr(9)) Then tempCh.RemoveAt(tempCh.IndexOf(Chr(9)))
                    ChLeft.AddRange(tempCh)
                Else
                    ChLeft.AddRange(Ch)
                End If
                N = 0
            Case "C"c, "D"c
                CD = C
                'original
                'Ch = [C] + [#1..#26] + ['A', 'B'] + IgnoreAbove
                Ch.Add(C)
                For i As Integer = 1 To 26
                    If Not Ch.Contains(Chr(i)) Then Ch.Add(Chr(i))
                Next
                If Not Ch.Contains("A"c) Then Ch.Add("A"c)
                If Not Ch.Contains("B"c) Then Ch.Add("B"c)
                If Not Ch.Contains(IgnoreAbove) Then Ch.Add(IgnoreAbove)

                ChLeft.AddRange(Ch)

                N = 13
            Case Else
                Exit Sub
        End Select

        Dim temp As Byte

        'original:
        'A := 1 - Byte ((WorldMap^ [X, Y - 1] in (Ch - IgnoreAbove)) or (Y = 0));
        Dim chTemp As New List(Of Char)
        chTemp.AddRange(Ch)
        If chTemp.Contains(IgnoreAbove) Then chTemp.Remove(IgnoreAbove)
        'Debug.WriteLine(String.Format("WorldMap(X, Y - 1) = {0} Test={1}", Asc(WorldMap(X, Y - 1)), chTemp.Contains(IgnoreAbove)))
        If (chTemp.Contains(WorldMap(X, Y - 1))) Or (Y = 0) Then
            temp = 1
        Else
            'Debug.WriteLine("temp=0")
            temp = 0
        End If
        A = 1 - temp 'A=1 or A=0

        'original
        'B := 2 * Byte (Not ((Y = NV - 1) or (WorldMap^ [X, Y + 1] in Ch)));
        'Debug.WriteLine(String.Format("WorldMap(X, Y + 1) = {0}", Asc(WorldMap(X, Y + 1))))
        If Not (Y = ScreenWidthInBlks - 1 Or Ch.Contains(WorldMap(X, Y + 1))) Then
            temp = 1
        Else
            temp = 0
        End If
        B = 2 * temp 'B=2 or B=0

        'original
        'L := 4 * Byte (Not ((X = 0) or (WorldMap^ [X - 1, Y] in ChLeft)));
        'Debug.WriteLine(String.Format("WorldMap(X - 1, Y) = {0}", Asc(WorldMap(X - 1, Y))))
        If Not (X = 0 Or ChLeft.Contains(WorldMap(X - 1, Y))) Then
            temp = 1
        Else
            temp = 0
        End If
        L = 4 * temp 'L=4 (block is the left-top most brick where the floor starts) or L=0

        'original
        'R := 8 * Byte (Not ((X = Options.XSize - 1) or (WorldMap^ [X + 1, Y] in Ch)));
        'Debug.WriteLine(String.Format("WorldMap(X + 1, Y) = {0}", Asc(WorldMap(X + 1, Y))))
        If Not (X = XSize - 1 Or Ch.Contains(WorldMap(X + 1, Y))) Then
            temp = 1
        Else
            temp = 0
        End If 'R=8 (block is the right-top most brick where the floor ends) or R=0. 

        R = 8 * temp

        'Debug.WriteLine(String.Format("A={0} B={1} L={2} R={3} N={4}", A, B, L, R, N))

        Select Case A + B + L + R
            Case 0 'A=B=L=R=0
                If (X > 0) And (Y > 0) Then
                    'Debug.WriteLine(String.Format("WorldMap(X - 1, Y - 1) = {0}", Asc(WorldMap(X - 1, Y - 1))))
                    If (Not Ch.Contains(WorldMap(X - 1, Y - 1))) Then
                        WorldMap(X, Y) = Chr(10 + N)
                        'Debug.WriteLine(String.Format("Return1 WorldMap(X, Y)={0}", Asc(WorldMap(X, Y))))
                        Exit Sub
                    End If
                End If
                If (X < XSize - 1) And (Y > 0) Then
                    'Debug.WriteLine(String.Format("WorldMap(X + 1, Y - 1) = {0}", Asc(WorldMap(X + 1, Y - 1))))
                    If Not (Ch.Contains(WorldMap(X + 1, Y - 1))) Then
                        WorldMap(X, Y) = Chr(11 + N)
                        'Debug.WriteLine(String.Format("Return2 WorldMap(X, Y)={0}", Asc(WorldMap(X, Y))))
                        Exit Sub
                    End If
                End If
                If (X > 0) And (Y < ScreenWidthInBlks - 1) Then
                    'Debug.WriteLine(String.Format("WorldMap(X - 1, Y + 1) = {0}", Asc(WorldMap(X - 1, Y + 1))))
                    If Not (Ch.Contains(WorldMap(X - 1, Y + 1))) Then
                        WorldMap(X, Y) = Chr(12 + N)
                        'Debug.WriteLine(String.Format("Return3 WorldMap(X, Y)={0}", Asc(WorldMap(X, Y))))
                        Exit Sub
                    End If
                End If
                If (X < XSize - 1) And (Y < ScreenWidthInBlks - 1) Then
                    'Debug.WriteLine(String.Format("WorldMap(X + 1, Y + 1) = {0}", Asc(WorldMap(X + 1, Y + 1))))
                    If Not (Ch.Contains(WorldMap(X + 1, Y + 1))) Then
                        WorldMap(X, Y) = Chr(13 + N)
                        'Debug.WriteLine(String.Format("Return4 WorldMap(X, Y)={0}", Asc(WorldMap(X, Y))))
                        Exit Sub
                    End If
                End If
                WorldMap(X, Y) = Chr(5 + N)
            Case 1 'A=1, B=L=R=0
                WorldMap(X, Y) = Chr(2 + N)
            Case 2 'B=2, A=L=R=0
                WorldMap(X, Y) = Chr(8 + N)
            Case 4 'L=4, A=B=R=0
                WorldMap(X, Y) = Chr(4 + N)
            Case 8 'R=8, A=B=L=0
                WorldMap(X, Y) = Chr(6 + N)
            Case 5 'A=1, L=4, B=R=0
                WorldMap(X, Y) = Chr(1 + N)
            Case 6 'B=2, L=4, A=R=0
                WorldMap(X, Y) = Chr(7 + N)
            Case 9 'A=1, R=8, B=L=0
                WorldMap(X, Y) = Chr(3 + N)
            Case 10 'B=2, R=8, A=L=0
                WorldMap(X, Y) = Chr(9 + N)
            Case Else
                WorldMap(X, Y) = Chr(5 + N)
        End Select

        'Debug.WriteLine(String.Format("Return5 WorldMap({0}, {1})={2} ChCount={3} ChLeftCount={4}", X, Y, Asc(WorldMap(X, Y)), Ch.Count, ChLeft.Count))
    End Sub

    ''' <summary>
    ''' The set of brick images to display wall/ground
    ''' </summary>
    Private Bricks(0 To 2) As Bitmap

    ''' <summary>
    ''' Build the set of bricks image used to display wall/ground
    ''' Call this before displaying each level.
    ''' </summary>
    ''' <param name="lvlProp">The properties for the current level</param>
    ''' <remarks>
    ''' Original: (FIGURES.PAS, function BuildWall)
    ''' with Options do
    ''' begin
    ''' case WallType1 of
    ''' ...
    ''' end;
    ''' ...
    ''' end;
    ''' </remarks>
    Private Sub BuildBricks(ByVal lvlProp As LevelProperties)
        'original: (FIGURES.PAS, function BuildWall)
        'with Options do
        'BuildWall := (WallType1 < 100);
        If lvlProp.Walls1 < 100 Then
            'Debug.WriteLine("Wall needs to be built first")

            LastAB = " "c
            LastCD = " "c
            LastEF = " "c

            For i As Integer = 0 To XSize - 1
                For j As Integer = 0 To ScreenWidthInBlks - 1
                    BuildWall(i, j)
                Next

                LastAB = AB
                LastAB = CD
                LastAB = EF
            Next
        Else
            Select Case lvlProp.Walls1
                Case 100
                    Bricks(0) = ReColor(My.Resources.BRICK0000, lvlProp.GroundColor1)
                    Bricks(1) = ReColor(My.Resources.BRICK0001, lvlProp.GroundColor1)
                    Bricks(2) = ReColor(My.Resources.BRICK0002, lvlProp.GroundColor1)
                Case 101
                    Bricks(0) = ReColor(My.Resources.BRICK1000, lvlProp.GroundColor1)
                    Bricks(1) = ReColor(My.Resources.BRICK1001, lvlProp.GroundColor1)
                    Bricks(2) = ReColor(My.Resources.BRICK1002, lvlProp.GroundColor1)
                Case 102
                    Bricks(0) = ReColor(My.Resources.BRICK2000, lvlProp.GroundColor1)
                    Bricks(1) = ReColor(My.Resources.BRICK2001, lvlProp.GroundColor1)
                    Bricks(2) = ReColor(My.Resources.BRICK2002, lvlProp.GroundColor1)
            End Select
        End If
    End Sub

    ''' <summary>
    ''' The set of pipe images
    ''' </summary>
    Private Pipes(0 To 3) As Bitmap

    ''' <summary>
    ''' Init the set of pipe images for display
    ''' </summary>
    ''' <param name="lvlProp">The properties for the current level</param>
    ''' <remarks>
    ''' Original:   procedure InitPipes (NewColor: Byte); (FIGURES.PAS)
    ''' </remarks>
    Private Sub InitPipes(ByVal lvlProp As LevelProperties)
        Pipes(0) = ReColor(My.Resources.PIPE000, lvlProp.Pipes)
        Pipes(1) = ReColor(My.Resources.PIPE001, lvlProp.Pipes)
        Pipes(2) = ReColor(My.Resources.PIPE002, lvlProp.Pipes)
        Pipes(3) = ReColor(My.Resources.PIPE003, lvlProp.Pipes)
    End Sub

    ''' <summary>
    ''' Fills an area on the level screen with the specified color
    ''' Originally: procedure Fill (X, Y, W, H: Integer; Attr: Integer); (VGA256.PAS)
    ''' </summary>
    Private Sub SolidFillImage(ByVal XPos As Integer, ByVal YPos As Integer, ByVal W As Integer, ByVal H As Integer, ByVal fillColor As Color)
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                CurrentLevelScreen.SetPixel(XPos + j, YPos + i, fillColor)
            Next
        Next
    End Sub

    ''' <summary>
    ''' Draw the specified an image onto the level screen at the specified position
    ''' </summary>
    Private Sub DrawImage(ByVal XPos As Integer, ByVal YPos As Integer, ByVal Fig As Bitmap)
        If Fig Is Nothing Then
            'Debug.WriteLine(String.Format("DrawImage(XPos {0}, YPos {1}) Nothing to draw", XPos, YPos))
            Return
        End If

        'Debug.WriteLine(String.Format("DrawImage(XPos {0}, YPos {1}, W {2}, H {3})", XPos, YPos, Fig.Width, Fig.Height))

        For i As Integer = 0 To Fig.Height - 1
            For j As Integer = 0 To Fig.Width - 1
                Dim thisColor As Color = Fig.GetPixel(j, i)

                If thisColor <> TransparencyColor Then
                    CurrentLevelScreen.SetPixel(XPos + j, YPos + i, thisColor)
                End If
            Next
        Next
    End Sub

    ''' <summary>
    ''' Draw the portion of the level screen having the specified coordinates.
    ''' Originally: procedure Redraw (X, Y: Integer); (FIGURES.PAS)
    ''' </summary>
    Private Sub DrawImagePartAt(ByVal X As Integer, ByVal Y As Integer, ByVal lvlProp As LevelProperties)
        Dim Ch As Char
        Dim Fig As Bitmap = Nothing
        Dim L, R, LS, RS As Boolean
        Dim XPos, YPos As Integer

        'original is (X*W). However, this will result in one blank vertical block on the left (why)
        XPos = (X - 1) * SpriteWidth

        YPos = Y * SpriteHeight
        Ch = WorldMap(X, Y)

        If (X >= 0) And (Y >= 0) And (Y < ScreenWidthInBlks) Then

            If (Ch <> Chr(0)) Then
                If (Ch = "%") And (lvlProp.Design = 4) Then
                    DrawSky(XPos, YPos, SpriteWidth, SpriteHeight \ 2)
                Else
                    DrawSky(XPos, YPos, SpriteWidth, SpriteHeight)
                End If
            End If

            If Ch = " " Then
                'Debug.WriteLine(String.Format("X={0} Y={1} blank", X, Y))
                Exit Sub
            End If

            If WorldMap(X, Y - 1) = Chr(18) Then
                'Debug.WriteLine(String.Format("({0}, {1}) Fig 4"))

                Fig = FigList(0, 4)
                DrawImage(XPos, YPos, Fig)

                Exit Sub
            End If

            Fig = Nothing

            Select Case Ch
                Case Chr(1) To Chr(26) 'draw the wall or the floor from a set of brick images
                    If X = 1 And Y = 12 Then
                        'Debug.WriteLine("here")
                    End If

                    If Ch > Chr(13) Then
                        Ch = Chr(Asc(Ch) - 13)
                    Else
                        If WorldMap(X - 1, Y) >= Chr(14) And WorldMap(X - 1, Y) <= Chr(26) Then
                            If Ch = Chr(1) Or Ch = Chr(4) Or Ch = Chr(7) Then
                                Fig = FigList(0, Asc(WorldMap(X - 1, Y)) - 14)

                                'Debug.WriteLine(String.Format("Wall DrawImage3({0}, {1}) Fig#{2}", XPos, YPos, Asc(WorldMap(X - 1, Y)) - 14))
                                DrawImage(XPos, YPos, Fig)
                            End If
                        Else
                            If WorldMap(X + 1, Y) >= Chr(14) And WorldMap(X + 1, Y) <= Chr(26) Then
                                If Ch = Chr(3) Or Ch = Chr(6) Or Ch = Chr(9) Then
                                    Fig = FigList(0, Asc(WorldMap(X + 1, Y)) - 14)

                                    'Debug.WriteLine(String.Format("Wall DrawImage2({0}, {1}) Fig #{2}", XPos, YPos, Asc(WorldMap(X + 1, Y)) - 14))
                                    DrawImage(XPos, YPos, Fig)
                                End If
                            End If
                        End If
                    End If

                    Dim ind = Asc(Ch) - 1
                    Fig = FigList(0, ind)
                    'Debug.WriteLine(String.Format("Here ch={2} coord ({0}, {1}, X={3}, Y={4})", XPos, YPos, Asc(Ch), X, Y))

                    If Not (Ch = Chr(1) Or Ch = Chr(3) Or Ch = Chr(4) Or Ch = Chr(6) Or Ch = Chr(7) Or Ch = Chr(9)) Then
                        'Debug.WriteLine(String.Format("Wall DrawImage1({0}, {1}) Fig# {2}", XPos, YPos, Asc(Ch) - 1))
                        DrawImage(XPos, YPos, Fig)
                        Fig = Nothing
                    End If
                Case "?"c
                    Fig = My.Resources.QUEST000 'Quest000
                Case "@"c
                    Fig = My.Resources.QUEST001

                Case "A"c
                    'the floor
                    L = (WorldMap(X - 1, Y) = "A") 'FALSE if left-most floor block
                    R = (WorldMap(X + 1, Y) = "A") 'FALSE if right-most floor block

                    If (X + Y) Mod 2 = 1 Then
                        RS = True
                        LS = False
                    Else
                        LS = True
                        RS = False
                    End If

                    If (LS And R) Then
                        Fig = Bricks(1)
                    Else
                        If (RS And L) Then
                            Fig = Bricks(2)
                        Else
                            Fig = Bricks(0)
                        End If
                    End If
                Case "I"c
                    Fig = My.Resources.BLOCK000
                Case "J"c
                    Fig = My.Resources.BLOCK001
                Case "K"c
                    Fig = My.Resources.NOTE000

                Case "X"c
                    Fig = My.Resources.XBLOCK000

                Case "W"c
                    Fig = My.Resources.WOOD000
                Case "="c
                    Fig = My.Resources.PIN000
                    If WorldMap(X, Y + 1) = CanHoldYou Then
                        DrawImage(XPos, YPos, Fig)
                    Else
                        UpSideDownImage(XPos, YPos, Fig)
                    End If
                    Fig = Nothing
                Case "0"c
                    Fig = Pipes(0)
                Case "1"c
                    Fig = Pipes(1)
                Case "2"c
                    Fig = Pipes(2)
                Case "3"c
                    Fig = Pipes(3)
                Case "*"c
                    Fig = My.Resources.COIN000

                Case "þ"c
                    If WorldMap(X, Y - 1) = "þ" Then
                        Fig = My.Resources.EXIT001
                    Else
                        Fig = My.Resources.EXIT000
                    End If
                Case "÷"c
                    If (WorldMap(X, Y - 1) = "ð") And (lvlProp.Design = 2) Then
                        Fig = My.Resources.SMTREE001
                        DrawImage(XPos, YPos, Fig)
                    End If
                    If WorldMap(X, Y - 1) = "ö" Then
                        If lvlProp.Design = 1 Then
                            Fig = My.Resources.WPALM000
                            DrawImage(XPos, YPos, Fig)
                        End If
                    End If
                    If (X = 0) Or (WorldMap(X - 1, Y) = Ch) Then
                        If WorldMap(X + 1, Y) = Ch Then
                            Fig = My.Resources.GRASS2000
                        Else
                            Fig = My.Resources.GRASS3000
                        End If
                    Else
                        If WorldMap(X + 1, Y) = Ch Then
                            Fig = My.Resources.GRASS1000
                        Else
                            Fig = My.Resources.GRASS3000
                        End If
                    End If
                Case "ð"c
                    Select Case lvlProp.Design
                        Case 1
                            If WorldMap(X, Y - 1) <> Ch Then
                                Fig = My.Resources.FENCE001
                            Else
                                Fig = My.Resources.FENCE000
                            End If
                        Case 2
                            If WorldMap(X, Y - 1) <> Ch Then
                                Fig = My.Resources.SMTREE000
                            Else
                                Fig = My.Resources.SMTREE001
                            End If
                    End Select

                Case "ö"c
                    Select Case lvlProp.Design
                        Case 1
                            Fig = My.Resources.WPALM000
                    End Select
                Case "ú"c
                    Select Case lvlProp.Design
                        Case 1
                            If WorldMap(X - 1, Y) = "ù" Then
                                Fig = My.Resources.PALM3000
                                DrawImage(XPos, YPos, Fig)
                            Else
                                If WorldMap(X + 1, Y) = "ù" Then
                                    Fig = My.Resources.PALM1000
                                    DrawImage(XPos, YPos, Fig)
                                End If
                            End If
                            Fig = My.Resources.PALM0000
                    End Select
                Case "ô"c
                    Select Case lvlProp.Design
                        Case 1
                            If WorldMap(X, Y + 1) = "ö" Then
                                Fig = My.Resources.WPALM000
                                DrawImage(XPos, YPos, Fig)
                            End If
                            Fig = My.Resources.PALM1000
                    End Select
                Case "ù"c
                    Select Case lvlProp.Design
                        Case 1
                            Fig = My.Resources.PALM2000
                    End Select
                Case "õ"c
                    Select Case lvlProp.Design
                        Case 1
                            If WorldMap(X, Y + 1) = "ö" Then
                                Fig = My.Resources.WPALM000
                                DrawImage(XPos, YPos, Fig)
                            End If
                            Fig = My.Resources.PALM3000
                    End Select

                Case "#"c
                    Select Case lvlProp.Design
                        Case 1
                            Fig = My.Resources.FALL000
                        Case 2
                            Select Case WorldMap(X, Y - 1).ToString
                                Case "#"
                                    DrawImage(XPos, YPos, My.Resources.TREE001)
                                Case "%"
                                    Fig = My.Resources.TREE000
                                    DrawImage(XPos, YPos, Fig)
                                    Fig = My.Resources.TREE003
                                Case Else
                                    Fig = My.Resources.TREE003
                            End Select
                        Case 3
                            Fig = My.Resources.WINDOW001
                        Case 4
                            Fig = My.Resources.LAVA000
                        Case 5
                            'originally: Fill (X, Y, W, H, $5) uses VGA color #5
                            'SolidFillImage(XPos, YPos, SpriteWidth, SpriteHeight, Color.FromArgb(186, 186, 255))
                    End Select
                Case "%"c
                    Select Case lvlProp.Design
                        Case 1
                            Fig = My.Resources.FALL001
                        Case 2
                            Select Case WorldMap(X, Y - 1).ToString
                                Case "%"
                                    DrawImage(XPos, YPos, My.Resources.TREE000)
                                Case "#"
                                    Fig = My.Resources.TREE001
                                    DrawImage(XPos, YPos, Fig)
                                    Fig = My.Resources.TREE002
                                Case Else
                                    Fig = My.Resources.TREE002
                            End Select
                        Case 3
                            Fig = My.Resources.WINDOW000
                        Case 4
                            Fig = My.Resources.LAVA001
                        Case 5
                            'TODO: Display appropriate type of Lava
                            'Select Case (X + LavaCounter \ 8) Mod 5
                            '    Case 0
                            '        Fig = My.Resources.LAVA2001
                            '    Case 1
                            '        Fig = My.Resources.LAVA2002
                            '    Case 2
                            '        Fig = My.Resources.LAVA2003
                            '    Case 3
                            '        Fig = My.Resources.LAVA2004
                            '    Case 4
                            '        Fig = My.Resources.LAVA2005
                            'End Select
                    End Select
            End Select

            If Fig IsNot Nothing Then
                'Debug.WriteLine(String.Format("XPos={0} YPos={1} Drawn", XPos, YPos))
                DrawImage(XPos, YPos, Fig)
            Else
                'Debug.WriteLine(String.Format("X={0} Y={1} Invalid", X, Y))
            End If

        End If
    End Sub

    ''' <summary>
    ''' Draw the image for the specified level, starting from the specified 'stage'.
    ''' </summary>
    Public Sub DrawLevel(ByVal LevelData As List(Of String), ByVal LevelProp As LevelProperties, ByVal stage As Integer)
        CurrentLevelScreen = New Bitmap(SpriteWidth * ScreenHeightInBlks, SpriteHeight * ScreenWidthInBlks)

        '1. prepare WorldMap

        'clear old data
        For i As Integer = 0 To WorldBufferWidth - 1
            For j As Integer = 0 To WorldBufferHeight - 1
                WorldBuffer(i, j) = " "c
            Next
        Next

        'Code snippet taken from procedure ReadWorld (var Map; W: WorldBufferPtr; var Opt); (BUFFERS.PAS)
        For lineCount As Integer = 1 To LevelData.Count - stage
            For i As Integer = 1 To ScreenWidthInBlks
                WorldMap(lineCount, ScreenWidthInBlks - i) = LevelData(stage + lineCount - 1)(i - 1)

                'Debug.WriteLine(String.Format("WorldMap({0}, {1}) = {2}", lineCount, ScreenWidthInBlks - i, Asc(WorldMap(lineCount, ScreenWidthInBlks - i))))
            Next
            WorldMap(lineCount, -EY1) = Chr(0)
            For i = 1 To EY2
                WorldMap(lineCount, ScreenWidthInBlks - 1 + i) = WorldMap(lineCount - 1, ScreenWidthInBlks - 1)

                'Debug.WriteLine(String.Format("WorldMap({0}, {1}) = {2}", lineCount, ScreenWidthInBlks - 1 + i, Asc(WorldMap(lineCount, ScreenWidthInBlks - 1 + i))))
            Next
        Next

        '2. Prepare other objects
        BuildBricks(LevelProp)

        InitPipes(LevelProp)

        InitWall(1, LevelProp.Walls1, LevelProp.GroundColor1, LevelProp.GroundColor2)
        InitWall(2, LevelProp.Walls2, LevelProp.GroundColor1, LevelProp.GroundColor2)
        InitWall(3, LevelProp.Walls3, LevelProp.GroundColor1, LevelProp.GroundColor2)

        '2. Draw the current screen for level.
        'Code snippet taken from procedure Pause; (PLAY.PAS)
        For X As Integer = XView \ SpriteWidth - 1 To XView \ SpriteWidth + ScreenHeightInBlks '{e.g. -1 to 16}
            For Y As Integer = 0 To ScreenWidthInBlks - 1 '{e.g. 0 to 12}
                'Debug.WriteLine(String.Format("DrawImagePartAt({0}, {1})", X, Y))

                DrawImagePartAt(X, Y, LevelProp)
            Next
        Next
    End Sub
#End Region

#Region "Read/Write Serialized Data"
    ''' <summary>
    ''' Save an object to XML file via object serialization.
    ''' Return TRUE if successful, FALSE if an error occurs
    ''' </summary>
    ''' <param name="datafile">Name of output XML file.</param>
    ''' <param name="ConfigObj">The object to be serialized.</param>
    ''' <param name="customSerializer">The optional serializer to be used during serialization. If missing, the default constructor of XmlSerializer will be used.</param>
    Public Function saveSerializedData(ByVal datafile As String, ByVal ConfigObj As Object, Optional ByVal customSerializer As XmlSerializer = Nothing) As Boolean
        Try
            Dim serializer As XmlSerializer
            If customSerializer Is Nothing Then
                serializer = New XmlSerializer(ConfigObj.GetType())
            Else
                serializer = customSerializer
            End If
            Dim writer As New StreamWriter(datafile)
            serializer.Serialize(writer, ConfigObj)
            writer.Close()

            Debug.WriteLine("saveSerializedData OK for " + datafile)
            Return True
        Catch ex As Exception
            Debug.WriteLine("saveSerializedData FAILED for " + datafile + ". " + ex.ToString)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Retrieve the data previously serialized into XML file via saveSerializedData. Return the de-serialized object if the process is successful.
    ''' </summary>
    ''' <param name="datafile">Name of an existing XML file.</param>
    ''' <param name="ConfigType">Type of data which was serialized. This is retrieved by calling obj.GetType or by GetType(obj)</param>
    ''' <param name="result">Indicates whether or not the de-serialization process was successful.</param>
    ''' <param name="customSerializer">The optional serializer to be used during de-serialization. If missing, the default constructor of XmlSerializer will be used.</param>
    Public Function readSerializedData(ByVal datafile As String, ByRef ConfigType As Type, ByRef result As Boolean, Optional ByVal customSerializer As XmlSerializer = Nothing) As Object
        Dim retConf As Object = Nothing 'configuration data to be returned

        'read from XML files
        Try
            Dim serializer As XmlSerializer
            If customSerializer Is Nothing Then
                serializer = New XmlSerializer(ConfigType)
            Else
                serializer = customSerializer
            End If

            Dim fs As New FileStream(datafile, FileMode.Open)
            retConf = serializer.Deserialize(fs)
            fs.Close()

            result = True
            Debug.WriteLine("readSerializedData OK for " + datafile)
        Catch ex As FileNotFoundException  'in case config file cannot be found
            result = False
            Debug.WriteLine("readSerializedData cannot find file: " + datafile + ". ")
        Catch ex As Exception 'other error
            result = False
            Debug.WriteLine("readSerializedData FAILED for " + datafile + ". " + ex.ToString)
        End Try

        Return retConf
    End Function
#End Region

    'original
    'FigList: Array [1 .. N1, 1 .. N2] of ImageBuffer;
    'As VB does not support non-zero array base, some modifications to the source code has to be made.
    Dim FigList(0 To N1 - 1, 0 To N2 - 1) As Bitmap

    ''' <summary>
    ''' Load the background object into array FigList
    ''' Original: procedure InitWall (N, WallType: Byte); (FIGURES.PAS)
    ''' </summary>
    Sub InitWall(ByVal N As Integer, ByVal WallType As Integer, ByVal GroundColor1 As Integer, ByVal GroundColor2 As Integer)
        Select Case WallType
            Case 0
                FigList(N - 1, 0) = My.Resources.GREEN000
                FigList(N - 1, 1) = My.Resources.GREEN001
                FigList(N - 1, 3) = My.Resources.GREEN002
                FigList(N - 1, 4) = My.Resources.GREEN003
                FigList(N - 1, 9) = My.Resources.GREEN004
            Case 1
                FigList(N - 1, 0) = My.Resources.SAND000
                FigList(N - 1, 1) = My.Resources.SAND001
                FigList(N - 1, 3) = My.Resources.SAND002
                FigList(N - 1, 4) = My.Resources.SAND003
                FigList(N - 1, 9) = My.Resources.SAND004
            Case 2
                FigList(N - 1, 0) = ReColor2(My.Resources.GREEN000, GroundColor1, GroundColor2)
                FigList(N - 1, 1) = ReColor2(My.Resources.GREEN001, GroundColor1, GroundColor2)
                FigList(N - 1, 3) = ReColor2(My.Resources.GREEN002, GroundColor1, GroundColor2)
                FigList(N - 1, 4) = ReColor2(My.Resources.GREEN003, GroundColor1, GroundColor2)
                FigList(N - 1, 9) = ReColor2(My.Resources.GREEN004, GroundColor1, GroundColor2)
            Case 3
                FigList(N - 1, 0) = My.Resources.BROWN000
                FigList(N - 1, 1) = My.Resources.BROWN001
                FigList(N - 1, 3) = My.Resources.BROWN002
                FigList(N - 1, 4) = My.Resources.BROWN003
                FigList(N - 1, 9) = My.Resources.BROWN004
            Case 4
                FigList(N - 1, 0) = My.Resources.GRASS000
                FigList(N - 1, 1) = My.Resources.GRASS001
                FigList(N - 1, 3) = My.Resources.GRASS002
                FigList(N - 1, 4) = My.Resources.GRASS003
                FigList(N - 1, 9) = My.Resources.GRASS004
            Case 5
                FigList(N - 1, 0) = My.Resources.DES000
                FigList(N - 1, 1) = My.Resources.DES001
                FigList(N - 1, 3) = My.Resources.DES002
                FigList(N - 1, 4) = My.Resources.DES003
                FigList(N - 1, 9) = My.Resources.DES004
        End Select

        FigList(N - 1, 2) = Mirror(FigList(N - 1, 0))
        FigList(N - 1, 5) = Rotate(FigList(N - 1, 3))
        FigList(N - 1, 8) = Rotate(FigList(N - 1, 0))
        FigList(N - 1, 7) = Rotate(FigList(N - 1, 1))
        FigList(N - 1, 6) = Rotate(FigList(N - 1, 2))
        FigList(N - 1, 10) = Mirror(FigList(N - 1, 9))
        FigList(N - 1, 11) = Rotate(FigList(N - 1, 10))
        FigList(N - 1, 12) = Mirror(FigList(N - 1, 11))
    End Sub

    ''' <summary>
    ''' Original: procedure Mirror (P1, P2: Pointer); (FIGURES.PAS)
    ''' </summary>
    ''' <param name="fig"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function Mirror(ByVal fig As Bitmap) As Bitmap
        If fig Is Nothing Then
            Debug.WriteLine("Mirror: nothing to mirror")
            Return Nothing
        End If

        'Return fig
        Dim picNew As New Bitmap(fig.Width, fig.Height)
        Dim i, j As Integer
        For i = 0 To fig.Width - 1
            For j = 0 To fig.Height - 1
                Dim col = fig.GetPixel(i, j)
                picNew.SetPixel(fig.Width - i - 1, j, col)
            Next
        Next
        Return picNew
    End Function

    ''' <summary>
    ''' Rotate an image by 180 degrees
    ''' Original: procedure Rotate (P1, P2: Pointer); (FIGURES.PAS)
    ''' </summary>
    Function Rotate(ByVal fig As Bitmap) As Bitmap
        If fig Is Nothing Then
            Debug.WriteLine("Rotate: nothing to rotate")
            Return Nothing
        End If

        Dim picNew As New Bitmap(fig.Width, fig.Height)
        Dim i, j As Integer
        For i = 0 To fig.Width - 1
            For j = 0 To fig.Height - 1
                picNew.SetPixel(fig.Width - i - 1, j, fig.GetPixel(i, j))
            Next
        Next
        Return picNew

        'Return fig
    End Function

    Sub DrawSky(ByVal XPos As Integer, ByVal YPos As Integer, ByVal W As Integer, ByVal H As Integer)
        'Debug.WriteLine(String.Format("DrawSky(XPos {0}, YPos {1}, W {2}, H {3})", XPos, YPos, W, H))
    End Sub

    Sub UpSideDownImage(ByVal XPos As Integer, ByVal YPos As Integer, ByVal img As Bitmap)
        Debug.WriteLine(String.Format("UpSideDownImage(XPos {0}, YPos {1})", XPos, YPos))
    End Sub
End Module
