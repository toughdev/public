﻿Public Class frmLevelViewer
    Private ActiveLevelData As List(Of String)
    Private ActiveLevelProp As LevelProperties

    Dim lockDrawLevel As New Object

    Private Sub HScrollBar1_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar1.Scroll
        SyncLock lockDrawLevel
            Try
                DrawLevel(ActiveLevelData, ActiveLevelProp, Me.HScrollBar1.Value)
                Me.picLevel.Image = CurrentLevelScreen

                Me.lblTotalBlock.Text = (ActiveLevelData.Count - 1).ToString
                Me.lblCurBlock.Text = String.Format("{0} to {1}", Me.HScrollBar1.Value, Me.HScrollBar1.Value + ScreenWidthInBlks - 1)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End SyncLock
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        If Me.OpenFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Me.txtFileName.Text = Me.OpenFileDialog1.FileName
            Me.txtFileName.SelectionStart = Me.txtFileName.Text.Length
        End If
    End Sub

    'read a level stored on disk. The levels must have been converted from WORLDS.PAS using LVL2BIN.PAS
    Private Sub btnRead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRead.Click
        Try
            Me.picLevel.Image = Nothing
            Me.HScrollBar1.Maximum = 0
            Me.HScrollBar1.Value = 0

            'read the level binary data
            ActiveLevelData = ReadLevelData(Me.txtFileName.Text)

            'read the level properties, expected to have the same name with .XML extension
            Dim isOK As Boolean = ReadLevelProperty(IO.Path.GetDirectoryName(Me.txtFileName.Text) + "\" + IO.Path.GetFileNameWithoutExtension(Me.txtFileName.Text) + ".XML", ActiveLevelProp)
            If Not isOK Then Throw New Exception("Unable to read level properties")

            If ActiveLevelData.Count > 0 Then
                Me.HScrollBar1.Maximum = Math.Max(ActiveLevelData.Count - ScreenWidthInBlks, 0)
                Me.HScrollBar1.Value = 0
                Me.lblTotalBlock.Text = (ActiveLevelData.Count - 1).ToString
                Me.lblCurBlock.Text = String.Format("{0} to {1}", Me.HScrollBar1.Value, Me.HScrollBar1.Value + ScreenWidthInBlks - 1)

                DrawLevel(ActiveLevelData, ActiveLevelProp, 0)

                Me.picLevel.Image = CurrentLevelScreen
            Else
                MessageBox.Show("Error: empty level data.")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim lvlProp As New LevelProperties
        lvlProp.Design = 5
        saveSerializedData("C:\TEMP\LEVEL1A.XML", lvlProp)
    End Sub
End Class