﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLevelViewer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picLevel = New System.Windows.Forms.PictureBox
        Me.HScrollBar1 = New System.Windows.Forms.HScrollBar
        Me.txtFileName = New System.Windows.Forms.TextBox
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.btnRead = New System.Windows.Forms.Button
        Me.lblTotal = New System.Windows.Forms.Label
        Me.lblTotalBlock = New System.Windows.Forms.Label
        Me.lblCurBlock = New System.Windows.Forms.Label
        Me.lblOffSet = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.picLevel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picLevel
        '
        Me.picLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picLevel.Location = New System.Drawing.Point(12, 55)
        Me.picLevel.Name = "picLevel"
        Me.picLevel.Size = New System.Drawing.Size(640, 364)
        Me.picLevel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picLevel.TabIndex = 0
        Me.picLevel.TabStop = False
        '
        'HScrollBar1
        '
        Me.HScrollBar1.LargeChange = 1
        Me.HScrollBar1.Location = New System.Drawing.Point(12, 431)
        Me.HScrollBar1.Maximum = 0
        Me.HScrollBar1.Name = "HScrollBar1"
        Me.HScrollBar1.Size = New System.Drawing.Size(638, 17)
        Me.HScrollBar1.TabIndex = 2
        '
        'txtFileName
        '
        Me.txtFileName.Location = New System.Drawing.Point(13, 4)
        Me.txtFileName.Name = "txtFileName"
        Me.txtFileName.Size = New System.Drawing.Size(470, 20)
        Me.txtFileName.TabIndex = 3
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(489, 4)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(84, 23)
        Me.btnBrowse.TabIndex = 4
        Me.btnBrowse.Text = "Browse"
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Filter = "Mario Level Data | *.lvl"
        '
        'btnRead
        '
        Me.btnRead.Location = New System.Drawing.Point(579, 4)
        Me.btnRead.Name = "btnRead"
        Me.btnRead.Size = New System.Drawing.Size(72, 23)
        Me.btnRead.TabIndex = 5
        Me.btnRead.Text = "Read"
        Me.btnRead.UseVisualStyleBackColor = True
        '
        'lblTotal
        '
        Me.lblTotal.Location = New System.Drawing.Point(552, 455)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(100, 19)
        Me.lblTotal.TabIndex = 7
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblTotalBlock
        '
        Me.lblTotalBlock.Location = New System.Drawing.Point(551, 479)
        Me.lblTotalBlock.Name = "lblTotalBlock"
        Me.lblTotalBlock.Size = New System.Drawing.Size(100, 19)
        Me.lblTotalBlock.TabIndex = 9
        Me.lblTotalBlock.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblCurBlock
        '
        Me.lblCurBlock.Location = New System.Drawing.Point(13, 479)
        Me.lblCurBlock.Name = "lblCurBlock"
        Me.lblCurBlock.Size = New System.Drawing.Size(100, 19)
        Me.lblCurBlock.TabIndex = 8
        '
        'lblOffSet
        '
        Me.lblOffSet.Location = New System.Drawing.Point(13, 455)
        Me.lblOffSet.Name = "lblOffSet"
        Me.lblOffSet.Size = New System.Drawing.Size(100, 19)
        Me.lblOffSet.TabIndex = 6
        Me.lblOffSet.Text = "Current"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(12, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(639, 21)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "View binary level data converted using LVL2BIN.PAS. Level options stored in a sep" & _
            "arate XML file having the same name."
        '
        'frmLevelViewer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(669, 499)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblTotalBlock)
        Me.Controls.Add(Me.lblCurBlock)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.lblOffSet)
        Me.Controls.Add(Me.btnRead)
        Me.Controls.Add(Me.btnBrowse)
        Me.Controls.Add(Me.txtFileName)
        Me.Controls.Add(Me.HScrollBar1)
        Me.Controls.Add(Me.picLevel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmLevelViewer"
        Me.Text = "Level Viewer"
        CType(Me.picLevel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picLevel As System.Windows.Forms.PictureBox
    Friend WithEvents HScrollBar1 As System.Windows.Forms.HScrollBar
    Friend WithEvents txtFileName As System.Windows.Forms.TextBox
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnRead As System.Windows.Forms.Button
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents lblTotalBlock As System.Windows.Forms.Label
    Friend WithEvents lblCurBlock As System.Windows.Forms.Label
    Friend WithEvents lblOffSet As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
