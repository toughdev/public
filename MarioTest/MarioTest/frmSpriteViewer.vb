﻿Imports System.IO
Imports System.Drawing.Drawing2D

Public Class frmSpriteViewer
    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        If Me.OpenFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Me.txtFileName.Text = Me.OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub btnSelectInputFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectInputFolder.Click
        If Me.FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Me.txtInputFolder.Text = Me.FolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Private Sub btnSelectOutputFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectOutputFolder.Click
        If Me.FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Me.txtOutputFolder.Text = Me.FolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Private Sub btnLoadPicture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoadPicture.Click
        Try
            Dim sprBitmap As Bitmap = ConvertSpriteToBitmap(Me.txtFileName.Text)

            Me.lblSpriteInfo.Text = String.Format("Height={0} Width={1}", sprBitmap.Height, sprBitmap.Width)

            'determine appropriate zoom factor
            Dim maxHeight = 112
            Dim maxWidth = 160
            Dim zoomRatio = Math.Min(maxWidth / sprBitmap.Width, maxHeight / sprBitmap.Height)
            Me.picSprite.Size = New Size(CInt(zoomRatio * sprBitmap.Width), CInt(zoomRatio * sprBitmap.Height))

            Me.picSprite.Image = sprBitmap
        Catch ex As Exception
            MessageBox.Show("Error: " + ex.Message)
        End Try
    End Sub

    Private Sub btnSavePicture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSavePicture.Click
        Try
            Dim sprBitmap As Bitmap = ConvertSpriteToBitmap(Me.txtFileName.Text)

            If Me.SaveFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then                
                sprBitmap.Save(Path.GetFileNameWithoutExtension(Me.SaveFileDialog1.FileName) + ".PNG", Imaging.ImageFormat.Png)
                MessageBox.Show("Sprite saved to PNG format.")
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " + ex.Message)
        End Try
    End Sub

    Private Sub btnConvertPNG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConvertPNG.Click
        Dim count As Integer = 0

        Try
            Cursor.Current = Cursors.WaitCursor

            For Each filename As String In Directory.GetFiles(Me.txtInputFolder.Text)
                Dim fileExt As String = Path.GetExtension(filename)

                Select Case fileExt
                    Case ".000", ".001", ".002", ".003", ".004", ".005", ".006"
                        count += 1
                        Dim bmp As Bitmap = ConvertSpriteToBitmap(filename)
                        Dim outfilename As String = Me.txtOutputFolder.Text + "\" + Path.GetFileName(filename).Replace(".", "") + ".PNG"
                        bmp.Save(outfilename, Imaging.ImageFormat.Png)
                        Debug.WriteLine(outfilename)
                End Select
            Next

            Cursor.Current = Cursors.Default
        Catch ex As Exception
            Cursor.Current = Cursors.Default

            MessageBox.Show("Error: " + ex.Message)
        End Try

        MessageBox.Show("Total Files Converted: " + count.ToString)
    End Sub
End Class
