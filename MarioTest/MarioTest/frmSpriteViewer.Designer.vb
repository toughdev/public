﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSpriteViewer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picSprite = New System.Windows.Forms.PictureBox
        Me.btnLoadPicture = New System.Windows.Forms.Button
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.btnConvertPNG = New System.Windows.Forms.Button
        Me.txtFileName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblSpriteInfo = New System.Windows.Forms.Label
        Me.btnSavePicture = New System.Windows.Forms.Button
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnSelectOutputFolder = New System.Windows.Forms.Button
        Me.btnSelectInputFolder = New System.Windows.Forms.Button
        Me.txtOutputFolder = New System.Windows.Forms.TextBox
        Me.txtInputFolder = New System.Windows.Forms.TextBox
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog
        CType(Me.picSprite, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'picSprite
        '
        Me.picSprite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picSprite.Location = New System.Drawing.Point(12, 12)
        Me.picSprite.Name = "picSprite"
        Me.picSprite.Size = New System.Drawing.Size(160, 112)
        Me.picSprite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picSprite.TabIndex = 0
        Me.picSprite.TabStop = False
        '
        'btnLoadPicture
        '
        Me.btnLoadPicture.Location = New System.Drawing.Point(264, 101)
        Me.btnLoadPicture.Name = "btnLoadPicture"
        Me.btnLoadPicture.Size = New System.Drawing.Size(92, 23)
        Me.btnLoadPicture.TabIndex = 3
        Me.btnLoadPicture.Text = "Load Picture"
        Me.btnLoadPicture.UseVisualStyleBackColor = True
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(178, 101)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(80, 23)
        Me.btnBrowse.TabIndex = 5
        Me.btnBrowse.Text = "Browse"
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Filter = "Sprite file | *.00?"
        '
        'btnConvertPNG
        '
        Me.btnConvertPNG.Location = New System.Drawing.Point(6, 94)
        Me.btnConvertPNG.Name = "btnConvertPNG"
        Me.btnConvertPNG.Size = New System.Drawing.Size(131, 23)
        Me.btnConvertPNG.TabIndex = 10
        Me.btnConvertPNG.Text = "Start Conversion"
        Me.btnConvertPNG.UseVisualStyleBackColor = True
        '
        'txtFileName
        '
        Me.txtFileName.Location = New System.Drawing.Point(178, 75)
        Me.txtFileName.Name = "txtFileName"
        Me.txtFileName.Size = New System.Drawing.Size(279, 20)
        Me.txtFileName.TabIndex = 11
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(179, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(278, 23)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Select a sprite (.000, .001, .002, ...) to view it. "
        '
        'lblSpriteInfo
        '
        Me.lblSpriteInfo.Location = New System.Drawing.Point(182, 40)
        Me.lblSpriteInfo.Name = "lblSpriteInfo"
        Me.lblSpriteInfo.Size = New System.Drawing.Size(275, 23)
        Me.lblSpriteInfo.TabIndex = 13
        '
        'btnSavePicture
        '
        Me.btnSavePicture.Location = New System.Drawing.Point(362, 101)
        Me.btnSavePicture.Name = "btnSavePicture"
        Me.btnSavePicture.Size = New System.Drawing.Size(95, 23)
        Me.btnSavePicture.TabIndex = 14
        Me.btnSavePicture.Text = "Save Picture"
        Me.btnSavePicture.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnSelectOutputFolder)
        Me.GroupBox1.Controls.Add(Me.btnSelectInputFolder)
        Me.GroupBox1.Controls.Add(Me.txtOutputFolder)
        Me.GroupBox1.Controls.Add(Me.txtInputFolder)
        Me.GroupBox1.Controls.Add(Me.btnConvertPNG)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 146)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(445, 123)
        Me.GroupBox1.TabIndex = 15
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Batch convert sprite into PNG"
        '
        'btnSelectOutputFolder
        '
        Me.btnSelectOutputFolder.Location = New System.Drawing.Point(313, 55)
        Me.btnSelectOutputFolder.Name = "btnSelectOutputFolder"
        Me.btnSelectOutputFolder.Size = New System.Drawing.Size(126, 23)
        Me.btnSelectOutputFolder.TabIndex = 19
        Me.btnSelectOutputFolder.Text = "Select Output Folder"
        Me.btnSelectOutputFolder.UseVisualStyleBackColor = True
        '
        'btnSelectInputFolder
        '
        Me.btnSelectInputFolder.Location = New System.Drawing.Point(313, 19)
        Me.btnSelectInputFolder.Name = "btnSelectInputFolder"
        Me.btnSelectInputFolder.Size = New System.Drawing.Size(126, 23)
        Me.btnSelectInputFolder.TabIndex = 18
        Me.btnSelectInputFolder.Text = "Select Input Folder..."
        Me.btnSelectInputFolder.UseVisualStyleBackColor = True
        '
        'txtOutputFolder
        '
        Me.txtOutputFolder.Location = New System.Drawing.Point(6, 55)
        Me.txtOutputFolder.Name = "txtOutputFolder"
        Me.txtOutputFolder.Size = New System.Drawing.Size(301, 20)
        Me.txtOutputFolder.TabIndex = 17
        '
        'txtInputFolder
        '
        Me.txtInputFolder.Location = New System.Drawing.Point(6, 19)
        Me.txtInputFolder.Name = "txtInputFolder"
        Me.txtInputFolder.Size = New System.Drawing.Size(301, 20)
        Me.txtInputFolder.TabIndex = 16
        '
        'frmSpriteViewer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(469, 278)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnSavePicture)
        Me.Controls.Add(Me.lblSpriteInfo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtFileName)
        Me.Controls.Add(Me.btnBrowse)
        Me.Controls.Add(Me.btnLoadPicture)
        Me.Controls.Add(Me.picSprite)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmSpriteViewer"
        Me.Text = "Sprite Viewer"
        CType(Me.picSprite, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picSprite As System.Windows.Forms.PictureBox
    Friend WithEvents btnLoadPicture As System.Windows.Forms.Button
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnConvertPNG As System.Windows.Forms.Button
    Friend WithEvents txtFileName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblSpriteInfo As System.Windows.Forms.Label
    Friend WithEvents btnSavePicture As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnSelectOutputFolder As System.Windows.Forms.Button
    Friend WithEvents btnSelectInputFolder As System.Windows.Forms.Button
    Friend WithEvents txtOutputFolder As System.Windows.Forms.TextBox
    Friend WithEvents txtInputFolder As System.Windows.Forms.TextBox
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog

End Class
