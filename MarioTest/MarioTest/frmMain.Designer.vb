﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSpriteView = New System.Windows.Forms.Button
        Me.btnLevelView = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'btnSpriteView
        '
        Me.btnSpriteView.Location = New System.Drawing.Point(13, 13)
        Me.btnSpriteView.Name = "btnSpriteView"
        Me.btnSpriteView.Size = New System.Drawing.Size(267, 23)
        Me.btnSpriteView.TabIndex = 0
        Me.btnSpriteView.Text = "View and Convert Sprites"
        Me.btnSpriteView.UseVisualStyleBackColor = True
        '
        'btnLevelView
        '
        Me.btnLevelView.Location = New System.Drawing.Point(13, 57)
        Me.btnLevelView.Name = "btnLevelView"
        Me.btnLevelView.Size = New System.Drawing.Size(267, 23)
        Me.btnLevelView.TabIndex = 1
        Me.btnLevelView.Text = "View Level Data"
        Me.btnLevelView.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 89)
        Me.Controls.Add(Me.btnLevelView)
        Me.Controls.Add(Me.btnSpriteView)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.Text = "Mario Test"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSpriteView As System.Windows.Forms.Button
    Friend WithEvents btnLevelView As System.Windows.Forms.Button
End Class
