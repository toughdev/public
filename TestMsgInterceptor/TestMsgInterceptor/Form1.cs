﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace TestMsgInterceptor
{
    public partial class Form1 : Form
    {
        // GUID of RTRule.dll. Must match with actual declaration in the file
        private string CLSID_RT = "{708C1547-D4AB-49d2-94D0-988431784506}";

        // the message window to watch for for notification from RTRule.dll
        private NewMsgWindow msgWin;

        public Form1()
        {
            InitializeComponent();

            // the Win32 message window. RTRule.dll will send a predefined message to this window
            // when a new incoming text message is received.
            msgWin = new NewMsgWindow();
            msgWin.Text = "NewSMSWatcher";
            msgWin.OnNewTextMessage += new NewMsgWindow.NewTextMessageEventHandler(msgWin_OnNewTextMessage);
        }

        bool msgWin_OnNewTextMessage(string sender, string messageText)
        {
            Debug.WriteLine(sender);
            Debug.WriteLine(messageText);
        }

        /// <summary>
        /// Get the application directory.
        /// </summary>
        public static string getAppDir()
        {
            return Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);
        }

        /// <summary>
        /// Create our own incoming message interceptor by extending MAPIRule from SDK Samples.
        /// This will work on all devices. The default .NET MessageInterceptor will fail to work on HTC HD2
        /// </summary>
        public void CreateInterceptorMethod2()
        {
            RemoveInterceptorMethod2();

            RegistryKey r = Registry.LocalMachine.CreateSubKey("\\Software\\Microsoft\\Inbox\\SVC\\SMS\\Rules");

            if (r != null)
            {
                try
                {
                    Debug.WriteLine("Creating Rule Method 2");
                    r.SetValue(CLSID_RT, 1);
                    Debug.WriteLine("Rule method 2 was created");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error creating Rule method 2: " + e.ToString());
                }

                r.Close();
            }

            RegistryKey r2 = Registry.ClassesRoot.CreateSubKey("\\CLSID\\" + CLSID_RT + "\\InprocServer32");

            if (r2 != null)
            {
                try
                {
                    Debug.WriteLine("Creating CLSID Method 2");
                    r2.SetValue("Default", getAppDir() + @"\RTRule.dll");
                    Debug.WriteLine("CLSID method 2 was created");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error creating CLSID method 2: " + e.ToString());
                }

                r2.Close();
            }
        }

        /// <summary>
        /// Clear our custom message interceptor registry setup
        /// </summary>
        public void RemoveInterceptorMethod2()
        {
            RegistryKey r = Registry.LocalMachine.CreateSubKey("\\Software\\Microsoft\\Inbox\\SVC\\SMS\\Rules");

            if (r != null)
            {
                try
                {
                    Debug.WriteLine("Deleting Rule Method 2");
                    r.DeleteValue(CLSID_RT);
                    Debug.WriteLine("Rule method 2 was deleted");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error deleting Rule method 2: " + e.ToString());
                }

                r.Close();
            }

            RegistryKey r2 = Registry.ClassesRoot.CreateSubKey("\\CLSID");

            try
            {
                r2.DeleteSubKeyTree(CLSID_RT);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error deleting Rule method 2: " + e.ToString());
            }

            if (r2 != null)
                r2.Close();
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIntercept_Click(object sender, EventArgs e)
        {
            this.CreateInterceptorMethod2();

            ResetUnit();
        }

        private void btnCancelIntercept_Click(object sender, EventArgs e)
        {
            this.RemoveInterceptorMethod2();

            ResetUnit();
        }


        /// <summary>
        /// Reboot the device
        /// </summary>
        public void ResetUnit()
        {
            int bytesReturned = 0;
            long IOCTL_HAL_REBOOT = 0x101003c;
            KernelIoControl(IOCTL_HAL_REBOOT, null, 0, null, 0, ref bytesReturned);
        }

        [DllImport("coredll.dll")]
        public static extern void KernelIoControl(long dwIoControlCode, int[] lpInBuf, long nInBufSize, byte[] lpOutBuf, byte nOutBufSize, ref int lpBytesReturned);
    }
}