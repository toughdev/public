﻿namespace TestMsgInterceptor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.btnIntercept = new System.Windows.Forms.Button();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.btnCancelIntercept = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            // 
            // btnIntercept
            // 
            this.btnIntercept.Location = new System.Drawing.Point(30, 26);
            this.btnIntercept.Name = "btnIntercept";
            this.btnIntercept.Size = new System.Drawing.Size(142, 20);
            this.btnIntercept.TabIndex = 0;
            this.btnIntercept.Text = "Start Intercept";
            this.btnIntercept.Click += new System.EventHandler(this.btnIntercept_Click);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Exit";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // btnCancelIntercept
            // 
            this.btnCancelIntercept.Location = new System.Drawing.Point(30, 78);
            this.btnCancelIntercept.Name = "btnCancelIntercept";
            this.btnCancelIntercept.Size = new System.Drawing.Size(142, 20);
            this.btnCancelIntercept.TabIndex = 1;
            this.btnCancelIntercept.Text = "Cancel Intercept";
            this.btnCancelIntercept.Click += new System.EventHandler(this.btnCancelIntercept_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.btnCancelIntercept);
            this.Controls.Add(this.btnIntercept);
            this.Menu = this.mainMenu1;
            this.Name = "Form1";
            this.Text = "Test Interceptor";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnIntercept;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.Button btnCancelIntercept;
    }
}

