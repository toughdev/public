// LaunchManager.cpp : Simulate Acer Aspire Launch Manager to turn on/off bluetooth/wifi, etc

#include "stdafx.h"


int _tmain(int argc, _TCHAR* argv[])
{
	/* Method 1: Turn on/off bluetooth by PostingMessage to WButton.EXE. 
		WButton.exe will in turn call OSD.exe to display the status on screen and call DeviceIOControl to work with the hardware */

	/*
	HWND hwndWButtion = FindWindow(L"Wireless class", NULL);

	// A message "Bluetooth is enabled/disabled" is displayed. This is achieved by WButton.exe calling 'OSD.EXE 0 20' (to disable) or 'OSD.EXE 0 19' (to enable)
	printf("Toggling Bluetooth Status...\n");
	if (hwndWButtion) PostMessage(hwndWButtion, 0x800A, 0, 1); else printf("Cannot find WButton.exe\n");

	//wait for request to be processed
	Sleep(5000);

	// A message "Wifi is enabled/disabled" is displayed. This is achieved by WButton.exe calling 'OSD.EXE 0 18' (to disable) or 'OSD.EXE 0 17' (to enable)
	printf("Toggling Wifi status...\n");
	if (hwndWButtion) PostMessage(hwndWButtion, 0x800A, 0, 0); else printf("Cannot find WButton.exe\n");

	printf("Done... Press any key to quit.\n");
	getchar();
	*/

	/* Method 2: Call DeviceIOControl directly */

	//obtain a handle to the wireless driver
	HANDLE hWireless = CreateFile(L"\\\\.\\Hotkey", 0x80000000, 1, NULL, 3, 0, NULL);

	if (hWireless)
	{
		//Turn off Bluetooth
		BYTE inBuffer[2]; 
		inBuffer[0] = 0;
		inBuffer[1] = 0;
		BYTE outBuffer[2]; 
		DWORD bytesRet;
		BOOL isOK;
		isOK = DeviceIoControl(hWireless, 0x2224C4, (void*) inBuffer, 2, (void*) outBuffer, 2, &bytesRet, NULL);

		MessageBox(GetActiveWindow(), L"Click OK to turn on Bluetooth", L"Bluetooth", MB_OK);

		//Turn on Bluetooth
		inBuffer[1] = 1;
		isOK = DeviceIoControl(hWireless, 0x2224C4, (void*) inBuffer, 2, (void*) outBuffer, 2, &bytesRet, NULL);

		//Turn off wifi
		inBuffer[0] = 1;
		inBuffer[1] = 0;
		isOK = DeviceIoControl(hWireless, 0x2224C4, (void*) inBuffer, 2, (void*) outBuffer, 2, &bytesRet, NULL);

		MessageBox(GetActiveWindow(), L"Click OK to turn on wifi", L"Bluetooth", MB_OK);

		//Turn on wifi
		inBuffer[0] = 1;
		inBuffer[1] = 1;
		isOK = DeviceIoControl(hWireless, 0x2224C4, (void*) inBuffer, 2, (void*) outBuffer, 2, &bytesRet, NULL);
	}

	return 0;
}

