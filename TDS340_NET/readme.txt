Sample code to control the Tektronix TDS 340 100MHz digital storage oscilloscope via the serial port

See details at http://www.toughdev.com/2014/01/programming-tektronix-tds-340-100mhz.html