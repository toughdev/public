Code for the 320x240 2.8" TFT LCD module that uses the ILI9320 controller.

See example usage at http://www.toughdev.com/2014/01/interfacing-hy28a-lcd-module-with.html