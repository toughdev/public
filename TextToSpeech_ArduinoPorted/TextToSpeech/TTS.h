/*
  Text To Speech syntesis library
  Copyright (c) 2008 Clive Webster.  All rights reserved.
  Nov. 29th 2009 - Modified to work with Arduino by Gabriel Petrut.
*/

/*
  The Text To Speech library uses Timer1 to generate the PWM
  output on digital pin 10. The output signal needs to be fed
  to an RC filter then through an amplifier to the speaker.
*/

#include "english.h"
#include "windows.h"

#define TEST_FILE_NAME "C:\\TEMP\\TEST.WAV"

/*
*  Speak an English command line of text
*/
void sayText(const char * );

/*
*  Speak a string of phonemes
*/
void speakPhonemes(const char * );

/*
Adjust the pitch
*/
void setPitch(byte pitch);

/*
Add a delay
*/
void writeDelay_ms(double duration);

// start and stop sound session
void soundOn(void);
void soundOff(void);

// Prototype functions for Arduino flash memory. To be removed
// INT16 pgm_read_word(const char* const * address);
const char* pgm_read_word(const char* const * address);
byte pgm_read_byte(const unsigned char* address);
byte pgm_read_byte(const char* address);