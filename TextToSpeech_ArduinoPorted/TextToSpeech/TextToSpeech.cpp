// TextToSpeech.cpp : Defines the entry point for the console application.
//
/*
7 December 2013

Demo Text to Speech programs. Adapted from the Arduino code to see how possible it is to put it onto a PIC.

References:

RoboVoice speech synthesizer: http://letsmakerobots.com/node/34940

SAM Software Automatic Mouth: http://simulationcorner.net/index.php?page=sam

S.A.M. as an Arduino Library - Software to make our robots talk: http://letsmakerobots.com/node/33388
SAM Source code: https://github.com/s-macke/SAM
*/

#include "stdafx.h"
#include "TTS.h"
#include <time.h>

typedef struct _TESTSTRUCT
{
	const char* txt;
} TESTSTRUCT;

int _tmain(int argc, _TCHAR* argv[])
{
	// testing functions
	TESTSTRUCT testStr;
	const char* str = "FIRST TEST1";
	testStr.txt = "Third Test";
	unsigned char c = 'A';
	int i;
	
	/* Seed the random-number generator with current time so that the numbers will be different every time we run. */
	srand((unsigned) time(NULL));

	printf("sizeof(char): %d\n", sizeof(char));
	printf("sizeof(boolean): %d\n", sizeof(boolean));
	printf("sizeof(WORD): %d\n", sizeof(WORD));
	printf("sizeof(int): %d\n", sizeof(int));

	const char* test = pgm_read_word(&str);
	printf("pgm_read_word: %s\n", test);

	test = pgm_read_word(&testStr.txt);
	printf("pgm_read_word2: %s\n", test);

	byte b = pgm_read_byte(str);
	printf("pgm_read_byte: %c\n", b);

	b = pgm_read_byte(&c);
	printf("pgm_read_byte2: %c\n", b);

	/* Display 5 random numbers. */
	for(i = 0; i < 5; i++)
		printf("%6d", rand());
	printf("\n");

	// Test File write	
	FILE* f1 = fopen("c:\\temp\\test.txt", "a");
	fprintf (f1, "Hello World: %d\n", rand());
	fclose (f1);

	// Perform Test to Speech
	remove(TEST_FILE_NAME);
	soundOn();

	setPitch(8);
	sayText("Hello  master! How are you doing");

	writeDelay_ms(500);	

	setPitch(1);
	sayText("I am fine, thank you.");

	soundOff();

	printf("Done");

	getc(stdin);

	return 0;
}

