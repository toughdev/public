/*
Text To Speech syntesis library
Copyright (c) 2008 Clive Webster.  All rights reserved.
Nov. 29th 2009 - Modified to work with Arduino by Gabriel Petrut.

https://github.com/jscrane/TTS
*/

/*
The Text To Speech library uses Timer1 to generate the PWM
output on digital pin 10. The output signal needs to be fed
to an RC filter then through an amplifier to the speaker.
*/

#include "stdafx.h"
#include "TTS.h"

char phonemes[128];	
char modifier[128];	// must be same size as 'phonemes'
char g_text[64];

char defaultPitch = 7;

// to output wave file
FILE* fOutWav = NULL;

// Lookup user specified pitch changes
static const char PitchesP[]  = { 1, 2, 4, 6, 8, 10, 13, 16 };

/* 
Placeholder functions for the Arduino flash memory functions
*/

// INT16 pgm_read_word(const char* const * address)
const char* pgm_read_word(const char* const * address)
{
	// return (*address)[0] + ((*address)[1] << 8);
	return *address;
}

byte pgm_read_byte(const unsigned char* address)
{
	return (byte)(address[0]);
}

byte pgm_read_byte(const char* address)
{
	return (byte)(address[0]);
}

/**
*
*  Find the single character 'token' in 'vocab'
*  and append its phonemes to dest[x]
*
*  Return new 'x'
*/

INT16 copyToken(char token,char * dest, INT16 x, const VOCAB* vocab){
	INT16 ph;
	const char* src;

	for(ph = 0; ph < numVocab; ph++){
		const char *txt = (const char *)pgm_read_word(&vocab[ph].txt);

		if(pgm_read_byte(&txt[0]) == token && pgm_read_byte(&txt[1])==0){

			src = (const char *)pgm_read_word(&vocab[ph].phoneme);
			while(pgm_read_byte(src)!=0){
				dest[x++] = pgm_read_byte(src);
				src++;
			}
			break;
		}
	}

	return x; 
}

byte whitespace(char c){
	return (c==0 || c==' ' || c==',' || c=='.' || c=='?' || c=='\'' || c=='!' || c==':' || c=='/') ? 1 : 0;
}

/**
*  Enter:
*  src => English text in upper case
*  vocab => VOCAB array
*  dest => address to return result
*  return 1 if ok, or 0 if error
*/
INT16 textToPhonemes(const char * src, const VOCAB* vocab, char * dest){
	INT16 outIndex = 0;// Current offset into dest
	INT16 inIndex = -1; // Starts at -1 so that a leading space is assumed

	while(inIndex==-1 || src[inIndex]!= 0){	// until end of text
		INT16 maxMatch=0;	// Max chars matched on input text
		INT16 numOut=0;	// Number of characters copied to output stream for the best match
		INT16 ph;
		boolean endsInWhiteSpace=FALSE;
		INT16 maxWildcardPos = 0;

		// Get next phoneme, P2
		for(ph = 0; ph < numVocab; ph++){
			INT16 y,x;
			char wildcard=0; // modifier
			INT16 wildcardInPos=0;
			boolean hasWhiteSpace=FALSE;
			const char* text = (const char*)pgm_read_word(&vocab[ph].txt); 
			const char* phon = (const char*)pgm_read_word(&vocab[ph].phoneme); 

			for(y=0;;y++){

				char nextVocabChar = pgm_read_byte(&text[y]);
				char nextCharIn = (y + inIndex==-1) ? ' ' : src[y + inIndex];
				if(nextCharIn>='a' && nextCharIn<='z'){
					nextCharIn = nextCharIn - 'a' + 'A';
				}

				if(nextVocabChar=='#' && nextCharIn >= 'A' && nextCharIn <= 'Z'){
					wildcard = nextCharIn; // The character equivalent to the '#'
					wildcardInPos=y;
					continue;
				}

				if(nextVocabChar=='_'){
					// try to match against a white space
					hasWhiteSpace=TRUE;
					if(whitespace(nextCharIn)){
						continue;
					}
					y--;
					break;
				}

				// check for end of either string
				if(nextVocabChar==0 || nextCharIn==0){
					break;
				}

				if(nextVocabChar != nextCharIn){
					break;
				}
			}

			// See if its the longest complete match so far
			if(y<=maxMatch || pgm_read_byte(&text[y])!=0){
				continue;
			}


			// This is the longest complete match
			maxMatch = y;
			maxWildcardPos = 0;
			x = outIndex; // offset into phoneme return data

			// Copy the matching phrase changing any '#' to the phoneme for the wildcard
			for(y=0;;y++){
				char c = pgm_read_byte(&phon[y]);
				if(c==0)
					break;
				if(c=='#'){
					if(pgm_read_byte(&phon[y+1])==0){
						// replacement ends in wildcard
						maxWildcardPos = wildcardInPos;
					}else{
						x = copyToken(wildcard,dest,x, vocab); // Copy the phonemes for the wildcard character
					}
				}else{
					dest[x++] = c;
				}
			}
			dest[x]=0;
			endsInWhiteSpace = hasWhiteSpace;

			// 14
			numOut = x - outIndex;	// The number of bytes added

		}// check next phoneme
		// 15 - end of vocab table

		//16
		if(endsInWhiteSpace==TRUE){
			maxMatch--;
		}

		//17
		if(maxMatch==0){
			//loggerP(PSTR("Mistake in SAY, no token for ")); 
			//logger(&src[inIndex]);
			//loggerCRLF();
			return 0;
		}

		//20
		outIndex += numOut;
		if(outIndex > 256-16){
			//loggerP(PSTR("Mistake in SAY, text too long\n"));
			return 0;
		}

		//21 
		inIndex += (maxWildcardPos>0) ? maxWildcardPos : maxMatch;
	}
	return 1; 
}


/**
*
*   Convert phonemes to data string
*   Enter: textp = phonemes string
*   Return: phonemes = string of sound data
*			modifier = 2 bytes per sound data
*
*/
INT16 phonemesToData(const char* textp, const PHONEME* phoneme){
	INT16 phonemeOut = 0; // offset into the phonemes array
	INT16 modifierOut = 0; // offset into the modifiers array
	INT16 L81=0; // attenuate
	INT16 L80=16;

	while(*textp != 0){
		// P20: Get next phoneme
		boolean anyMatch=FALSE;
		INT16 longestMatch=0;
		INT16 ph;
		INT16 numOut=0;	// The number of bytes copied to the output for the longest match


		// Get next phoneme, P2
		for(ph = 0; ph<numPhoneme; ph++){
			INT16 numChars;

			// Locate start of next phoneme 
			const char* ph_text = (const char*)pgm_read_word(&phoneme[ph].txt);

			// Set 'numChars' to the number of characters
			// that we match against this phoneme
			for(numChars=0;textp[numChars]!=0 ;numChars++){

				// get next input character and make lower case
				char nextChar = textp[numChars];
				if(nextChar>='A' && nextChar<='Z'){
					nextChar = nextChar - 'A' + 'a';
				}

				if(nextChar!=pgm_read_byte(&ph_text[numChars])){
					break;
				}
			}

			// if not the longest match so far then ignore
			if(numChars <= longestMatch) continue;

			if(pgm_read_byte(&ph_text[numChars])!=0){
				// partial phoneme match
				continue;
			}

			// P7: we have matched the whole phoneme
			longestMatch = numChars;

			// Copy phoneme data to 'phonemes'
			{
				const char* ph_ph = (const char*)pgm_read_word(&phoneme[ph].phoneme);
				for(numOut=0; pgm_read_byte(&ph_ph[numOut])!= 0; numOut++){
					phonemes[phonemeOut+numOut] = pgm_read_byte(&ph_ph[numOut]);
				}
			}
			L81 = pgm_read_byte(&phoneme[ph].attenuate)+'0';
			anyMatch=TRUE; // phoneme match found

			modifier[modifierOut]=-1;
			modifier[modifierOut+1]=0;

			// Get char from text after the phoneme and test if it is a numeric
			if(textp[longestMatch]>='0' && textp[longestMatch]<='9'){
				// Pitch change requested
				modifier[modifierOut] = pgm_read_byte(&PitchesP[textp[longestMatch]-'1'] );
				modifier[modifierOut+1] = L81;
				longestMatch++;
			}

			// P10
			if(L81!='0' && L81 != L80 && modifier[modifierOut]>=0){
				modifier[modifierOut - 2] = modifier[modifierOut];
				modifier[modifierOut - 1] = '0';
				continue;
			}

			// P11
			if( (textp[longestMatch-1] | 0x20) == 0x20){
				// end of input string or a space
				modifier[modifierOut] = (modifierOut==0) ? 16 : modifier[modifierOut-2];
			}

		} // next phoneme

		// p13
		L80 = L81;
		if(longestMatch==0 && anyMatch==FALSE){
			//loggerP(PSTR("Mistake in speech at "));
			//logger(textp);
			//loggerCRLF();
			return 0;
		}

		// Move over the bytes we have copied to the output
		phonemeOut += numOut;

		if(phonemeOut > sizeof(phonemes)-16){
			//loggerP(PSTR("Line too long\n"));
			return 0;
		}

		// P16

		// Copy the modifier setting to each sound data element for this phoneme
		if(numOut > 2){
			INT16 count;
			for(count=0; count != numOut; count+=2){
				modifier[modifierOut + count + 2] = modifier[modifierOut + count];
				modifier[modifierOut + count + 3] = 0;
			}
		}
		modifierOut += numOut;

		//p21
		textp += longestMatch;
	}

	phonemes[phonemeOut++]='z';
	phonemes[phonemeOut++]='z';
	phonemes[phonemeOut++]='z';
	phonemes[phonemeOut++]='z';

	while(phonemeOut < sizeof(phonemes)){
		phonemes[phonemeOut++]=0;
	}

	while(modifierOut < sizeof(modifier)){
		modifier[modifierOut++]=-1;
		modifier[modifierOut++]=0;
	}

	return 1;  
}



byte random2(void){
	return (rand() % 0xFF);
}

void soundOff(void){
    if (fOutWav) fclose (fOutWav);
}

void soundOn(void){
	fOutWav = fopen(TEST_FILE_NAME, "ab");
}

void writeDelay(byte duration){
	int i;

	for (i=0; i< duration;i++)
	{
		byte buffer[1];
		buffer[0] = 0;
		fwrite(buffer, 1, 1, fOutWav);
	}
}

void writeDelay_ms(double duration){
	for (int i=0; i<duration/1.5;i++)
	{
		// 160 kHz = 160KByte/sec.
		// 255 byte = 1.5ms
		writeDelay(255);
	}
}

#define PWM_TOP 510
static const byte Volume[8] = {0, PWM_TOP * 0.07, PWM_TOP * 0.14, PWM_TOP * 0.21, PWM_TOP * 0.29, PWM_TOP * 0.36, PWM_TOP * 0.43, PWM_TOP * 0.5};

void writeSound(byte b, byte duration)
{
	// output file readable at 160kHz, 8-bit unsigned mono

	// char buf[50];
	// sprintf(buf, "Byte: %d, Duration: %d\n", b, duration);
	// OutputDebugStringA(buf);

	byte buffer[1];
	// buffer[0] = Volume[b>>1];
	buffer[0] = b*11;

	int i;
	for (i=0; i<duration;i++)
	{
		fwrite(buffer, 1, 1, fOutWav);
	}
}

byte playTone(byte soundNum,byte soundPos,char pitch1, char pitch2, byte count, byte volume){
	const byte* soundData = &SoundData[soundNum * 0x40];
	while(count-- > 0 ){
		byte s;

		s = pgm_read_byte(&soundData[soundPos & 0x3fu]);

		writeSound((byte)(s & volume), pitch1);
		writeSound((byte)((s>>4) & volume), pitch2);

		soundPos++;
	}
	return soundPos & 0x3fu; 
}

void play(byte duration, byte soundNumber){
	while(duration-- != 0){
		playTone(soundNumber,random2(), 7,7, 10, 15);
	}
}

/******************************************************************************
* User API
******************************************************************************/  
void setPitch(byte pitch){
	defaultPitch = pitch;
}


/*
*  Speak a string of phonemes
*/
void speakPhonemes(const char* textp){
	byte 
		phonemeIn,				// offset into text
		byte2,
		modifierIn,				// offset into stuff in modifier
		punctuationPitchDelta;	// change in pitch due to fullstop or question mark
	char byte1;
	char phoneme;
	const SOUND_INDEX* soundIndex;
	byte sound1Num;			// Sound data for the current phoneme
	byte sound2Num;			// Sound data for the next phoneme
	byte sound2Stop;			// Where the second sound should stop
	char pitch1;			// pitch for the first sound
	char pitch2;			// pitch for the second sound
	short i;
	byte sound1Duration;		// the duration for sound 1

	if(phonemesToData(textp,s_phonemes)){
		// _630C
		byte1=0;
		punctuationPitchDelta=0;

		//Q19
		for(phonemeIn=0,modifierIn=0;phonemes[phonemeIn]!=0; phonemeIn+=2, modifierIn+=2){
			byte	duration;	// duration from text line
			byte SoundPos;	// offset into sound data
			byte fadeSpeed=0;

			phoneme=phonemes[phonemeIn];
			if(phoneme=='z'){
				writeDelay(15);
				continue;
			}else if(phoneme=='#'){
				continue;
			}else{

				// Collect info on sound 1
				soundIndex = &SoundIndex[phoneme - 'A'];
				sound1Num = pgm_read_byte(&soundIndex->SoundNumber);
				byte1 = pgm_read_byte(&soundIndex->byte1);
				byte2 = pgm_read_byte(&soundIndex->byte2);

				duration = phonemes[phonemeIn+1] - '0';	// Get duration from the input line
				if(duration!=1){
					duration<<=1;
				}
				duration += 6;							// scaled duration from the input line (at least 6)

				sound2Stop = 0x40>>1;


				pitch1 = modifier[modifierIn];
				if(modifier[modifierIn + 1]==0 || pitch1==-1){
					pitch1 = 10;
					duration -= 6;
				}else if(modifier[modifierIn + 1]=='0' || duration==6){
					duration -= 6;
				}


				//q8
				pitch2 = modifier[modifierIn+2];
				if(modifier[modifierIn + 3]==0 || pitch2 == -1){
					pitch2 = 10;
				}

				//q10

				if(byte1<0){
					sound1Num = 0;
					random2();
					sound2Stop=(0x40>>1)+2;
				}else{
					// is positive
					if(byte1==2){
						// 64A4
						// Make a white noise sound !
						byte volume;					// volume mask
						volume = (duration==6) ? 15 : 1;  /// volume mask
						for(duration <<= 2; duration>0; duration--){
							playTone(sound1Num,random2(),8,12,11, volume);
							// Increase the volume
							if(++volume==16){
								volume = 15;	// full volume from now on
							}

						}
						continue;

					}else{
						//q11
						if(byte1 != 0){
							writeDelay(25);
						}
					}
				}

			}

			// 6186
			pitch1 += defaultPitch + punctuationPitchDelta;
			if(pitch1<1){
				pitch1=1;
			}

			pitch2 += defaultPitch + punctuationPitchDelta;
			if(pitch2<1){
				pitch2=1;
			}

			// get next phoneme
			phoneme=phonemes[phonemeIn + 2];

			if(phoneme==0 || phoneme=='z'){
				if(duration==1){
					writeDelay(60);
				}
				phoneme='a';	
			}else{
				// s6
				if(byte2 != 1){
					byte2 = (byte2 + pgm_read_byte(&SoundIndex[phoneme-'A'].byte2))>>1;
				}

				if(byte1 < 0 || pgm_read_byte(&SoundIndex[phoneme-'A'].byte1) != 0){
					phoneme ='a'; 
				}
			}

			// S10
			sound2Num = pgm_read_byte(&SoundIndex[phoneme-'A'].SoundNumber);

			sound1Duration = 0x80;			// play half of sound 1
			if(sound2Num==sound1Num){
				byte2 = duration;
			}

			// S11
			if( (byte2>>1) == 0 ){
				sound1Duration = 0xff;				// play all of sound 1
			}else{
				// The fade speed between the two sounds
				fadeSpeed = (sound1Duration + (byte2>>1))/byte2;

				if(duration==1){
					sound2Stop = 0x40;	// dont play sound2
					sound1Duration = 0xff;			// play all of sound 1
					pitch1 = 12;
				}
			}

			SoundPos = 0;
			do{
				byte sound1Stop = (sound1Duration>>2) & 0x3fu;
				byte sound1End = min(sound1Stop , sound2Stop);

				if( sound1Stop != 0 ){
					SoundPos = playTone(sound1Num,SoundPos,pitch1,pitch1, sound1End, 15);
				}

				// s18
				if(sound2Stop != 0x40){
					SoundPos = playTone(sound2Num,SoundPos,pitch2,pitch2, (byte)(sound2Stop - sound1End), 15);
				}

				//s23
				if(sound1Duration!=0xff && duration<byte2){
					// Fade sound1 out
					sound1Duration -= fadeSpeed;
					if( sound1Duration >= (byte)0xC8){
						sound1Duration=0;	// stop playing sound 1
					}
				}

				// Call any additional sound
				if(byte1==-1){
					play(3,30);	// make an 'f' sound
				}else if(byte1==-2){
					play(3,29);	// make an 's' sound
				}else if(byte1==-3){
					play(3,33);	// make a 'th' sound
				}else if(byte1==-4){
					play(3,27);	// make a 'sh' sound
				}

			}while(--duration!=0);

			// Scan ahead to find a '.' or a '?' as this will change the pitch
			punctuationPitchDelta=0;
			for(i=6; i>0; i--){
				char next = phonemes[phonemeIn + (i * 2)];
				if(next=='i'){
					// found a full stop
					punctuationPitchDelta = 6 - i; // Lower the pitch
				}else if(next=='h'){
					// found a question mark
					punctuationPitchDelta = i - 6; // Raise the pitch
				}
			}

			if(byte1 == 1){
				writeDelay(25);
			}


		} // next phoneme

	}
}

/*
*   Speak an English command line of text
*/
void sayText(const char * original){
	INT16 i;
	if(textToPhonemes(original, s_vocab, phonemes)){
		// copy string from phonemes to text 
		for(i = 0;phonemes[i]!=0;i++){
			g_text[i]=phonemes[i];
		}
		while(i<sizeof(g_text)){
			g_text[i++]=0;
		}
		speakPhonemes(g_text);
	}
}