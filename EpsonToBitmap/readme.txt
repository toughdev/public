Contains code to convert a printer output file containing the escape codes intended for an old Epson printer into a bitmap file. Part of a project to emulate a parallel port printer and save the output onto an SD card.

See details at http://www.toughdev.com/2014/02/capturing-data-from-tektronix-1230.html