An all-in-one project in which I use the Microchip USB stack on a PIC18F4550 and develop a .NET application to display status of peripherals (temperature/pressure sensors, USB keyboards, real-time-clock ICs, etc) connected to the PIC. 

This project is inspired from http://www.waitingforfriday.com/index.php/Building_a_PIC18F_USB_device

See details at http://www.toughdev.com/2013/05/custom-usb-hid-device-using-pic18f4550.html