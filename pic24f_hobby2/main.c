#include <p24hj128gp202.h>
#include "delay.h"
#include "uart.h"
#include "st7735.h"
#include "spi_lib.h"
#include "colors.h"
#include "MDD File System/FSIO.h"
#include "MDD File System/SD-SPI.h"
#include "string.h"
#include "vs1053.h"
#include "sd_helper.h"

// http://dangerousprototypes.com/2012/04/26/bpv3-ftdi-uart-demo-wiki/
// for possible headers, refer to C:\Program Files (x86)\Microchip\MPLAB C30\support\PIC24F\h\p24FJ64GA002.h
// _CONFIG2(FNOSC_FRCPLL & OSCIOFNC_OFF & POSCMOD_NONE & I2C1SEL_PRI) // Internal FRC OSC = 8 MHz x 4PLL = 32MHz. Set to OSCIOFNC_OFF for clock out at RA3
// _CONFIG1(JTAGEN_OFF & GCP_OFF & GWRP_OFF & COE_OFF & FWDTEN_OFF & ICS_PGx1) //turn off junk we don't need

// http://www.microchip.com/forums/m653681.aspx

/* Various Write Protect functions diabled */ 
_FBS(BWRP_WRPROTECT_OFF) 
_FSS(SWRP_WRPROTECT_OFF) 
_FGS(GWRP_OFF & GCP_OFF) 

/* Enable Primary Clock with PLL; Start with user selected clock source */ 
/*
Device has onboard unconfigurable brown-own reset if voltage is below certain threshold
*/ 
_FOSCSEL(FNOSC_FRCPLL & IESO_OFF) // Internal Fst RC with PLL, then start with user-selected oscillator source
_FOSC(POSCMD_NONE & OSCIOFNC_ON & IOL1WAY_OFF) // Primary Oscillator Disabled, Disable Clock Output at RA3 (Pin 10), 
_FWDT(FWDTEN_OFF) // Watch Dog Timer is disabled
_FPOR(FPWRT_PWR128) // Power on Reset time is 128 ms
_FICD(ICS_PGD1 & JTAGEN_OFF) // ICD at PGD1, JTAG off

// count of the 256Hz timer use for duration counting
unsigned long timerCount = 0;

void initIO()
{
	// Set of clock frequency
	// Using internal oscillator = 7.37MHz
	// Running Frequency = 7.37 * 32 / 1 / 2 / 4 = 30 MHz
	// Output at RA3 = Fosc / 2 = 92 / 2 = 15 MHz 
	// As the onboard RC oscillator is approximate, and UART baudrate (U1BRG) is dependent on the FOSC
	// experiments should be done to find a reasonable frequency for UART to work with standard baudrate (56700, 38400, etc)
	CLKDIVbits.FRCDIV = 0;     	// FRC divide by 1
	CLKDIVbits.PLLPOST = 1;		// PLL divide by 4
	CLKDIVbits.PLLPRE = 0;		// PLL divide by 2
	PLLFBDbits.PLLDIV = 32; 	// Freq. Multiplier (default is 50)

	// connect to MAX232
	ADPCFG = 0xFFFF;				// all digital
	ODCB  = 0b0000001000000000; // UART RB9 connected to MAX232 and needs pull up resistor as MAX232 works at 5V, not 3.3V!
	TRISB = 0b0001000100101000;	// PORTB for output, except RB3 (VS1053 DREQ), RB8 (UART RX) and RB5 (SD card input)
	TRISA = 0b0000000000000000; // PORTA all output

	// UART Module
	RPINR18bits.U1RXR = 8; //pin RP8 for RX
	RPOR4bits.RP9R   = 3; //pin RP9, 3 for UART1 transmit, 5 for UART2 RECV
	InitUART();

	// assign SPI pins (MOSI - RB6, Clock RB7). No MISO for the 5110 LCD (write-only)
	// this must be done after UART init, other UART will not work
	// suspected problem ie because we did not assigned all SPI pins.
	__builtin_write_OSCCONL(OSCCON & 0xBF);  //unlock RPn assign registers 

	// for VS1053 Module SPI (SPI2)
	RPINR22bits.SDI2R = 12;           	//Assign MISO to RP12.
	RPOR1bits.RP2R = 10;             	//Assign MOSI to RP2.
	RPOR6bits.RP13R = 11;             	//Assign SCK2OUT to RP13.

	// for SD card (SP1)
	RPINR20bits.SDI1R = 5; 		 		// RP5, MISO - (Master In Slave Out)
	RPOR5bits.RP10R = 0x7;			 	// MOSI at RP10
	RPOR2bits.RP4R = 0x8;			 	// SCK1OUT at RP4	
	
	__builtin_write_OSCCONL(OSCCON | 0x40);  //lock RPn assign registers  
}

void showSDCardInfo()
{
	char buf[50];

	SendUARTStr("\nDetecting card info...");

	// get SD card information
	SDCARD_INFO cardInfo = getCardInfo();

	sprintf(buf, "Label :  %s", cardInfo.volumeLabel);
	SendUARTStr(buf);
	ST7735_drawString(0, 20, buf, colYellow, 1);	

	sprintf(buf, "Format:  %s", cardInfo.fsTypeStr);
	SendUARTStr(buf);
	ST7735_drawString(0, 30, buf, colYellow, 1);	

	sprintf(buf, "Size :   %ld MB", cardInfo.totalSize / 1024 / 1024);
	SendUARTStr(buf);
	ST7735_drawString(0, 40, buf, colYellow, 1);	

	sprintf(buf, "Free :   %ld MB", cardInfo.freeSize / 1024 / 1024);
	SendUARTStr(buf);
	ST7735_drawString(0, 50, buf, colYellow, 1);	
}

void ListCardRootDir()
{
	char buf[50];
	unsigned int filesCount = 0;
	unsigned int dirCount = 0;

	SendUARTStr("\n\nRoot Directory Contents:");

	// the volume label is the first File Record in the root directory
	SearchRec rec;
	int res = 0;
	res = FindFirst("*.*", ATTR_MASK, &rec); //8.3 filename
	// res = wFindFirst("*.*", ATTR_MASK, &rec); //Long filename
	if (res == 0)
	{
		if (rec.attributes & ATTR_VOLUME)
		{
			// volume label, ignore
		}
		else
		SendUARTStr(rec.filename);

		do
		{
			res = FindNext(&rec);
			// res = wFindNext(&rec);
			
			if (res == 0)
			{
				unsigned int year = (rec.timestamp >> 25) + 1980;
				unsigned char month = (rec.timestamp & 0x1FFFFFF) >> 21;
				unsigned char day = (rec.timestamp & 0x1FFFFF) >> 16;
				unsigned char hour = (rec.timestamp & 0xFFFF) >> 11;
				unsigned char minute = (rec.timestamp & 0x7FF) >> 5;
				unsigned char second = (rec.timestamp & 0x1F) * 2;
				
				sprintf(buf, "%s%c%12s %10ld %02d/%02d/%04d %02d:%02d:%02d", 
				(rec.attributes & ATTR_DIRECTORY) ? "\e[0;34m" : "\e[0;30m", 
				(rec.attributes & ATTR_DIRECTORY) ? '~' : ' ', rec.filename, rec.filesize, day, month, year, hour, minute, second);
				
				if (rec.attributes & ATTR_DIRECTORY)
				dirCount += 1;
				else
				filesCount += 1;

				SendUARTStr(buf);
			}
		}
		while (res == 0);

		/*
		http://stackoverflow.com/questions/15763259/unix-timestamp-to-fat-timestamp
		
		The DOS date/time format is a bitmask:
		
		24                16                 8                 0
		+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+
		|Y|Y|Y|Y|Y|Y|Y|M| |M|M|M|D|D|D|D|D| |h|h|h|h|h|m|m|m| |m|m|m|s|s|s|s|s|
		+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+
		\___________/\________/\_________/ \________/\____________/\_________/
		year        month       day      hour       minute        second
		
		The year is stored as an offset from 1980. 
		Seconds are stored in two-second increments. 
		(So if the "second" value is 15, it actually represents 30 seconds.)
		*/
	}

	sprintf(buf, "\e[0;31m%d file(s) %d dir(s) on card.", filesCount, dirCount);
	SendUARTStr(buf);
	SendUARTStr("\e[0;30mDone.");
}

void LCDTest()
{
	ST7735_drawString(0, 50, "LARGE", colCyan, 3);
	ST7735_drawString(0, 100, "Big", colPurple, 7);

	ST7735_fillCircle(20, 20, 10, colRed);
	ST7735_drawCircle(60, 60, 30, colCyan);

	ST7735_drawRect(30, 100, 40, 20, colWhite);
	ST7735_fillRect(60, 70, 10, 10, colMagenta);

	ST7735_drawVerticalLine(5, 5, 20, colBrown);
	ST7735_drawHorizontalLine(5, 5, 20, colBrown);

	ST7735_drawLine(20, 20, 60, 60, colWhite);
}

/*
It pulls 32 byte chunks from the SD card and throws them at the VS1053
We monitor the DREQ (data request pin). If it goes low then we determine if
we need new data or not. If yes, pull new from SD card. Then throw the data
at the VS1053 until it is full.

Support MP3/WMA/OGG/WAV. Only Midi type 0 (single track) is supported. 
Download sample MIDI file from http://home.centurytel.net/milleman/
Type 1 and 2 must be converted. FLAC supported only via software plugin. 
http://www.vlsi.fi/en/support/software/vs10xxplugins.html 
*/
void vs1053_play_file(char* filename) {
	char buf[30];
	FSFILE * pointer;
	unsigned int y;

#define DATA_BUFFER_LENGTH 32
	unsigned char buffer[DATA_BUFFER_LENGTH];
	unsigned int bytesRead = 0;
	long totalBytes = 0;


	sprintf(buf, "Loading file %s. Press ESC to stop.", filename);	
	SendUARTStr(buf);

	sprintf(buf, "PLAYING: %s", filename);
	ST7735_drawString(0, 10, buf, colYellow, 1);

	pointer = FSfopen (filename, FS_READ);
	if (pointer == NULL)
	{
		ST7735_drawString(0, 20, "FILE LOAD ERROR", colRed, 1);

		SendUARTStr("Error opening file.");
		return;
	}

	int need_data = TRUE; 

	while(1) {
		while(!MP3_DREQ_IN) { 
			//DREQ is low while the receive buffer is full
			//You can do something else here, the buffer of the MP3 is full and happy.
			//Maybe set the volume or test to see how much we can delay before we hear audible glitches

			//If the MP3 IC is happy, but we need to read new data from the SD, now is a great time to do so
			if(need_data == TRUE) {
				
				bytesRead = FSfread(buffer, 1, DATA_BUFFER_LENGTH, pointer);
				totalBytes += bytesRead;

				if(bytesRead == 0) { //Try reading 32 new bytes of the song
					//Oh no! There is no data left to read!
					//Time to exit
					break;
				}
				need_data = FALSE;
			}

			if (U1STAbits.URXDA != 0)
			{
				// UART data available, check if ESC key is pressed
				char c = 0;
				RecUART(&c);

				if (c==27)
				{
					// set SM_CANCEL (bit 3) of SCI_MODE to stop playback properly
					unsigned int curSci = vs1053_ReadRegister(SCI_MODE);
					curSci = curSci | 0b1000;
					vs1053_WriteRegister(SCI_MODE, curSci >> 8, curSci & 0xFF);

					goto stopPlayback;
				}
			}
		}

		if(need_data == TRUE){ //This is here in case we haven't had any free time to load new data
			bytesRead = FSfread(buffer, 1, DATA_BUFFER_LENGTH, pointer);
			totalBytes += bytesRead;

			if(bytesRead == 0) { //Go out to SD card and try reading 32 new bytes of the song
				//Oh no! There is no data left to read!
				//Time to exit
				break;
			}
			need_data = FALSE;
		}

		//Once DREQ is released (high) we now feed 32 bytes of data to the VS1053 from our SD read buffer
		MP3_XDCS = 0; //Select Data
		for(y = 0 ; y < DATA_BUFFER_LENGTH ; y++)
		spi2Write(buffer[y]); // Send SPI byte

		MP3_XDCS = 1; //Deselect Data
		need_data = TRUE; //We've just dumped 32 bytes into VS1053 so our SD read buffer is empty. Set flag so we go get more data
	}

	while(!MP3_DREQ_IN) ; //Wait for DREQ to go high indicating transfer is complete

stopPlayback:

	MP3_XDCS = 1; //Deselect Data

	ST7735_drawString(0, 20, "Playback Done", colYellow, 1);

	sprintf(buf, "Read file done (%ld bytes)", totalBytes);	
	SendUARTStr(buf);

	FSfclose (pointer);
}

void GetFilenameEntered(char* nameEntered)
{
	char c;
	char filename[255];
	strcpy(filename, "");
	do
	{
		RecUART(&c);
		SendUART(c);
		
		if (c == '\r')
		SendUARTStr("\n");
		else if (c == 8 && strlen(filename) > 0)
		filename[strlen(filename)-1] = 0;
		else 
		sprintf(filename, "%s%c", filename, c);	
	}
	while (c != '\r');

	strcpy(nameEntered, filename);
}

void PlayAudioFile()
{
	char buf[50];
	SendUARTStr("\nType file name in root directory to be played, followed by the ENTER key.");
	
	char filename[255];
	GetFilenameEntered(filename);
	if (strlen(filename) > 0)
	{
		sprintf(buf, "Filename is %s", filename);
		SendUARTStr(buf);

		vs1053_play_file(filename);
	}
	else
	SendUARTStr("No filename entered.");
}

void LoadPhotoFile()
{
	char buf[50];
	SendUARTStr("\nType photo name in root directory to be loaded, followed by the ENTER key.");
	
	char filename[255];
	GetFilenameEntered(filename);
	if (strlen(filename) > 0)
	{
		sprintf(buf, "Filename is %s", filename);
		SendUARTStr(buf);

		loadBitmapToLCD(filename);
	}
	else
	SendUARTStr("No filename entered.");
}

void StartRecording()
{
	char buf[70];
	FSFILE * pointer;
	unsigned long wordsWaiting = 0;
	unsigned long totalBytes = 0;

	SendUARTStr("\nStarting Recording. Press ESC to stop.");
	ST7735_drawString(0, 20, "WRITING ...", colYellow, 1);

	// Create a file
	pointer = FSfopen ("VOICE1.WAV", "w");
	if (pointer == NULL)
	{	
		SendUARTStr("File creation error.");
		ST7735_drawString(0, 30, "FILE ERROR...", colRed, 1);
		return;
	}

	/* Recording Sampling Rate - 8kHz */
	vs1053_WriteRegisterW(SCI_AICTRL0, 8000);  // 8000 kHz
	vs1053_WriteRegisterW(SCI_AICTRL1, 0);     // use AGC
	vs1053_WriteRegisterW(SCI_AICTRL2, 0);	   // maximum range
	vs1053_WriteRegisterW(SCI_AICTRL3, 0b101); // bit 2 - linear PCM mode (on)/IMA PCM (off),  bit 0/1: 0 = joint stereo (common AGC), 1 = dual channel (separate AGC), 2 = left channel, 3 = right channel. Left channel is either MIC or LINE1 depending on the SCI_MODE register.

	/* Reset VS1053 */
	unsigned int sciValue = vs1053_ReadRegister(SCI_MODE);
	sciValue = sciValue | 0b0001000000000100; // SM_RESET at bit 2, SM_ADPCM at bit 12
	sciValue = sciValue & 0b1011111111111111; // MICP (=0)/LINE1 (=1) at bit 14
	vs1053_WriteRegisterW(SCI_MODE, sciValue); 
	delay_ms(100);
	
	// write patch (datasheet page 53) - for VS1053B only
	vs1053_WriteRegisterW(SCI_WRAMADDR, 0x8010);
	vs1053_WriteRegisterW(SCI_WRAM, 0x3e12);
	vs1053_WriteRegisterW(SCI_WRAM, 0xb817);
	vs1053_WriteRegisterW(SCI_WRAM, 0x3e14);
	vs1053_WriteRegisterW(SCI_WRAM, 0xf812);
	vs1053_WriteRegisterW(SCI_WRAM, 0x3e01);
	vs1053_WriteRegisterW(SCI_WRAM, 0xb811);
	vs1053_WriteRegisterW(SCI_WRAM, 0x0007);
	vs1053_WriteRegisterW(SCI_WRAM, 0x9717);
	vs1053_WriteRegisterW(SCI_WRAM, 0x0020);
	vs1053_WriteRegisterW(SCI_WRAM, 0xffd2);
	vs1053_WriteRegisterW(SCI_WRAM, 0x0030);
	vs1053_WriteRegisterW(SCI_WRAM, 0x11d1);
	vs1053_WriteRegisterW(SCI_WRAM, 0x3111);
	vs1053_WriteRegisterW(SCI_WRAM, 0x8024);
	vs1053_WriteRegisterW(SCI_WRAM, 0x3704);
	vs1053_WriteRegisterW(SCI_WRAM, 0xc024);
	vs1053_WriteRegisterW(SCI_WRAM, 0x3b81);
	vs1053_WriteRegisterW(SCI_WRAM, 0x8024);
	vs1053_WriteRegisterW(SCI_WRAM, 0x3101);
	vs1053_WriteRegisterW(SCI_WRAM, 0x8024);
	vs1053_WriteRegisterW(SCI_WRAM, 0x3b81);
	vs1053_WriteRegisterW(SCI_WRAM, 0x8024);
	vs1053_WriteRegisterW(SCI_WRAM, 0x3f04);
	vs1053_WriteRegisterW(SCI_WRAM, 0xc024);
	vs1053_WriteRegisterW(SCI_WRAM, 0x2808);
	vs1053_WriteRegisterW(SCI_WRAM, 0x4800);
	vs1053_WriteRegisterW(SCI_WRAM, 0x36f1);
	vs1053_WriteRegisterW(SCI_WRAM, 0x9811);
	vs1053_WriteRegisterW(SCI_WRAMADDR, 0x8028);
	vs1053_WriteRegisterW(SCI_WRAM, 0x2a00);
	vs1053_WriteRegisterW(SCI_WRAM, 0x040e);

	unsigned long startTime = timerCount;

	/* Main loop */
	while (1) {

		/* Example microcontroller Stop key = end recording */
		if (U1STAbits.URXDA != 0)
		{
			// UART data available, check if ESC key is pressed
			char c = 0;
			RecUART(&c);
			
			if (c==27)
			{
				// ESC to stop recording	
				unsigned int sciValue = vs1053_ReadRegister(SCI_MODE);
				sciValue = sciValue | 0b0000000000000100;
				sciValue = sciValue & 0b1110111111111111;
				vs1053_WriteRegisterW(SCI_MODE, sciValue); // SM_RESET at bit 2, SM_ADPCM at bit 12
				delay_ms(100);

				break;
			}
		}


		/* See how many 16-bit words there are waiting in the VS1053 buffer */
		wordsWaiting = vs1053_ReadRegister(SCI_HDAT1);
		if (wordsWaiting >= 1024)
		{
			unsigned long oldCount = timerCount;
			sprintf(buf, "Writing in progress (%lu bytes). Total Written: %lu bytes (%lus)", wordsWaiting*2, totalBytes, (timerCount - startTime) / 256);
			SendUARTStr(buf);

			unsigned int t;
			unsigned int i;
			for (i=0; i<wordsWaiting; i++) {
				t = vs1053_ReadRegister(SCI_HDAT0);
				
				unsigned int sendBuffer[] = {t}; //little endian, least significant byte first
				FSfwrite (sendBuffer, 2, 1, pointer);
			}
			totalBytes += wordsWaiting*2;		

			sprintf(buf, "Writing done (%lu sec)", (timerCount - oldCount) / 256);
			SendUARTStr(buf);
		}	
	}

	FSfclose (pointer);

	ST7735_drawString(0, 30, "WRITING DONE...", colWhite, 1);

	sprintf(buf, "Recording done (%lu bytes - %lus)", totalBytes, (timerCount - startTime) / 256);
	SendUARTStr(buf);
}

void ShowUARTHelp()
{
	SendUARTStr("\n? - Show this help page");
	SendUARTStr("\\ - Clear Screen");
	SendUARTStr("| - Show SD Card Info");
	SendUARTStr("@ - List Card Root Directory");
	SendUARTStr("% - Draw Graphics on LCD");
	SendUARTStr("* - Load Photo on Card");
	SendUARTStr("& - Play Hello on VS1053");
	SendUARTStr("[ - Start Recording using VS1053");
	SendUARTStr("] - Test SD Card Write");
	SendUARTStr("= - Play Audio File on Card");
}

void __attribute__((interrupt, auto_psv))  _T4Interrupt (void) 
{ 
	timerCount++;

	LATAbits.LATA2=!LATAbits.LATA2;
	
	_T4IF = 0; //Clear the Flag
}

void initTmr4()
{
	// enable timer 4 
	T4CONbits.TON = 0; // Disable Timer
	T4CONbits.TCS = 0; // Select internal instruction cycle clock
	T4CONbits.TGATE = 0; // Disable Gated Timer mode
	T4CONbits.TCKPS = 0b00; // Select 1:1 Prescaler
	TMR4 = 0x00; // Clear timer register
	PR4 = 62500; // Load the period value, 256Hz
	IPC6bits.T4IP = 0x01; // Set Timer Interrupt Priority Level
	IFS1bits.T4IF = 0; // Clear Timer Interrupt Flag
	IEC1bits.T4IE = 1; // Enable Timer interrupt
	T4CONbits.TON = 1; // Start Timer 
}

void TestSdCardWrite()
{
	char buf[50];
	FSFILE * pointer;


	unsigned long i = 0;
	unsigned long TOTAL_BYTES = 1048576;

	unsigned long startTime = timerCount;

	sprintf(buf, "\nTesting Starts (%lu bytes).", TOTAL_BYTES);
	SendUARTStr(buf);

	ST7735_drawString(0, 20, "WRITING TO SD CARD ...", colYellow, 1);

	// Create a file
	pointer = FSfopen ("TEST.BIN", "w");
	if (pointer == NULL)
	{	
		SendUARTStr("File creation error.");
		ST7735_drawString(0, 30, "FILE ERROR...", colRed, 1);
		return;
	}
	
	for (i=0; i< TOTAL_BYTES; i++)
	{
		unsigned char sendBuffer[] = {0xFF};
		FSfwrite (sendBuffer, 1, 1, pointer);

		/*
		if (i%100000==0)
		{
			sprintf(buf, "Testing in progress (%lu bytes written).", i);
			SendUARTStr(buf);			
		}
		*/
	}
	
	FSfclose (pointer);

	ST7735_drawString(0, 30, "TESTING DONE...", colWhite, 1);

	unsigned long duration = (timerCount - startTime) / 256;
	sprintf(buf, "Testing done (%lu bytes - %lus @ %luKB/s)", TOTAL_BYTES,  duration, TOTAL_BYTES / duration / 1024);
	SendUARTStr(buf);
}

int main()
{	
	char buf[255];

	initIO();
	initTmr4();

	SendUARTStr("\n\nPIC24HJ128GP202 Hello World.");

	// LCD
	ST7735_initR();
	ST7735_fillScreen(colBlue);

	// maximum 21 chars per line
	ST7735_drawString(0, 0, "ST7735 & SD Card Test", colWhite, 1);

	// Test the SD Card
	ST7735_drawString(0, 10,     "Detecting SD Card", colYellow, 1);
	BOOL isOK = initSDCard();
	if (!isOK)
	{
		ST7735_drawString(0, 20, "SD Card Not Found!" , colRed, 1);	
	}
	else
	{
		showSDCardInfo();
	}

	// SPI2 for VS1053. 1MHz SPI Clock Frequency initially. Will be changed to 4 MHz by vs1053_setup after setup is completed.
	spi2Init(0b00010010);     // primary scaler 4:1, secondary scaler 4:1 @ 1MHz 
	VS1053_INFO retVal = vs1053_setup();
	if (retVal.isPresent)
	{
		sprintf(buf, "VS1053 OK: v%u (%u)", retVal.vsVersion, retVal.MP3Mode);
		ST7735_drawString(0, 60, buf, colYellow, 1);

		SendUARTStr(buf);
		SendUARTStr("Playing Hello!");
		vs1053_sayHello();

		// vs1053_play_file("SONG.MP3");
	}
	else
	SendUARTStr("VS1053 Error.");	

	ShowUARTHelp();

	while(1)
	{
		// Echo back UART character
		char c;
		RecUART(&c);
		SendUART(c);

		switch(c)
		{
		case '\r':
			SendUART('\n');
			break;
		case '?':
			ShowUARTHelp();
			break;
		case '|':
			ST7735_fillScreen(colBlue);
			showSDCardInfo();
			break;
		case '\\':
			ST7735_fillScreen(colBlue);
			break;
		case '@':
			ListCardRootDir();
			break;
		case '%':
			ST7735_fillScreen(colBlue);
			LCDTest();
			break;
		case '=':
			ST7735_fillScreen(colBlue);
			PlayAudioFile();
			break;
		case '*':
			ST7735_fillScreen(colBlue);
			LoadPhotoFile();
			break;
		case '[':
			ST7735_fillScreen(colBlue);
			StartRecording();
			break;
		case ']':
			ST7735_fillScreen(colBlue);
			TestSdCardWrite();
			break;
		case '&':
			SendUARTStr("Playing Hello!");
			vs1053_sayHello();
			break;
		default:
			break;								
		}
	}
}
