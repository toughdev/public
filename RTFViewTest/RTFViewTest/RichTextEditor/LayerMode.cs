﻿namespace AgileNotesTouch
{
    using System;

    public enum LayerMode
    {
        Writing,
        Drawing,
        Smart,
        Links,
        SmartLinks
    }
}

