﻿namespace AgileNotesTouch
{
    using System;

    public enum ViewMode
    {
        Typing,
        Writing,
        Drawing
    }
}

