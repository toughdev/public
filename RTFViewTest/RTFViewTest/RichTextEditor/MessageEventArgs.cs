﻿namespace GuiInterop
{
    using System;

    public class MessageEventArgs : EventArgs
    {
        private bool m_fHandled = true;
        private IntPtr m_hwnd;
        private uint m_lParam;
        private uint m_msg;
        private int m_ret;
        private uint m_wParam;

        public MessageEventArgs(IntPtr hwnd, uint msg, uint wParam, uint lParam)
        {
        }

        public bool Handled
        {
            get
            {
                return this.m_fHandled;
            }
            set
            {
                this.m_fHandled = value;
            }
        }

        public IntPtr Hwnd
        {
            get
            {
                return this.m_hwnd;
            }
        }

        public uint lParam
        {
            get
            {
                return this.m_lParam;
            }
        }

        public uint Msg
        {
            get
            {
                return this.m_msg;
            }
        }

        public int ReturnValue
        {
            get
            {
                return this.m_ret;
            }
            set
            {
                this.m_ret = value;
            }
        }

        public uint wParam
        {
            get
            {
                return this.m_wParam;
            }
        }
    }
}

