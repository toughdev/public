﻿namespace GuiInterop
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    public class WindowHost : Control
    {
        private WindowsAPIs.WindowProcCallback m_delegate;
        private IntPtr m_hwnd;
        private IntPtr m_wndprocReal;

        public event MessageEventHandler MessageReceived;

        public WindowHost(IntPtr hwndNativeControl)
        {
            Debug.Assert(hwndNativeControl != IntPtr.Zero, "Control should have a handle created by now");
            Debug.Assert(base.Handle != IntPtr.Zero, "Control should have a handle created by now");
            this.m_hwnd = hwndNativeControl;
            this.m_delegate = new WindowsAPIs.WindowProcCallback(this.WnProc);
            this.m_wndprocReal = WindowsAPIs.SetWindowLong(this.m_hwnd, -4, Marshal.GetFunctionPointerForDelegate(this.m_delegate));
        }

        public WindowHost(string strClassName, uint wsEx, uint ws)
        {
            Debug.Assert(base.Handle != IntPtr.Zero, "Control should have a handle created by now");
            //this.m_hwnd = WindowsAPIs.CreateWindowExW(wsEx, strClassName, null, (ws | WS_CHILDWINDOW) | WS_VISIBLE, 0, 0, 0, 0, base.Handle, 0, 0, 0);
            this.m_hwnd = WindowsAPIs.CreateWindowExW(wsEx, strClassName, null, (ws | WS_CHILDWINDOW | ES_MULTILINE |WS_VSCROLL) | WS_VISIBLE, 0, 0, 0, 0, base.Handle, 0, 0, 0);
            if (this.m_hwnd == IntPtr.Zero)
            {
                Debug.Assert(false, "Class not registered");
                throw new OutOfMemoryException();
            }
            this.m_delegate = new WindowsAPIs.WindowProcCallback(this.WnProc);
            this.m_wndprocReal = WindowsAPIs.SetWindowLong(this.m_hwnd, -4, Marshal.GetFunctionPointerForDelegate(this.m_delegate));
        }

        protected override void OnGotFocus(EventArgs e)
        {
            WindowsAPIs.SetFocus(this.m_hwnd);
        }

        protected void OnMessageReceived(MessageEventArgs e)
        {
            if (this.MessageReceived != null)
            {
                this.MessageReceived(this, e);
            }
        }

        protected override void OnResize(EventArgs e)
        {
            WindowsAPIs.SetWindowPos(this.m_hwnd, 0, 0, 0, base.ClientSize.Width, base.ClientSize.Height, 4);
        }

        protected override void OnTextChanged(EventArgs e)
        {
            WindowsAPIs.SetWindowText(this.m_hwnd, this.Text);
        }

        private int WnProc(IntPtr hwnd, uint msg, uint wParam, uint lParam)
        {
            MessageEventArgs e = new MessageEventArgs(hwnd, msg, wParam, lParam);
            this.OnMessageReceived(e);
            if (!((msg != 2) || e.Handled))
            {
                WindowsAPIs.SetWindowLong(this.m_hwnd, -4, this.m_wndprocReal);
            }
            if (e.Handled)
            {
                return WindowsAPIs.CallWindowProc(this.m_wndprocReal, hwnd, msg, wParam, lParam);
            }
            return e.ReturnValue;
        }

        public IntPtr NativeHandle
        {
            get
            {
                return this.m_hwnd;
            }
        }

        private const int ES_READONLY = 0x800;
        private const int WS_CHILDWINDOW = 0x40000000;
        private const int WS_VISIBLE = 0x10000000;
        private const uint CW_USEDEFAULT = 0x80000000;
        private const int ES_AUTOHSCROLL = 0x80;
        private const int ES_AUTOVSCROLL = 0x40;
        private const int WS_VSCROLL = 0x00200000;
        private const int WS_HSCROLL = 0x00100000;
        private const int ES_MULTILINE = 4;
    }
}

