﻿namespace AgileNotesTouch
{
    using System;

    public enum PageStyleMode
    {
        Yellow,
        DottedLines,
        GridLines,
        RuledLines,
        TopMargin,
        LeftMargin,
        TopLeftMargin,
        None
    }
}

