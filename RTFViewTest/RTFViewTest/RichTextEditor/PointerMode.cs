﻿namespace AgileNotesTouch
{
    using System;

    public enum PointerMode
    {
        Select,
        Pen,
        Space
    }
}

