﻿namespace AgileNotesTouch
{
    using System;

    public enum InkFontColor
    {
        Black,
        Gray,
        LightGray,
        White,
        Red,
        LightGreen,
        Blue,
        Aqua,
        Fuchsia,
        Yellow,
        Purple,
        Green,
        Navy,
        Teal,
        Maroon,
        Olive
    }
}

