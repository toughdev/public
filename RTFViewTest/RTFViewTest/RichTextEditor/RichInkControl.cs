﻿namespace AgileNotesTouch
{
    using GuiInterop;
    using Microsoft.WindowsCE.Forms;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Windows.Forms;
    using System.Diagnostics;

    /// <summary>
    /// Wrapper around the native RickInk control for use in a .NET application.
    /// </summary>
    /// <remarks>
    /// MD created this class by decompiling AgileNotes Touch 3.0 (http://www.agilitylab.com/)
    /// and make some motifications.
    /// </remarks>
    public class RichInkControl : UserControl
    {
        private static byte[] buff;
        private IContainer components = null;
        private static int fpos;
        private static int fsize;
        private static FileStream fstream;
        private ActionMode m_Action;
        private LayerMode m_Layer;
        private PageStyleMode m_PageStyle;
        private PointerMode m_Pointer;
        private WindowHost m_RichInk;
        private ViewMode m_View;
        private bool m_VoiceBar;
        private WrapMode m_Wrap;
        private int m_Zoom;

        public RichInkControl()
        {
            this.InitializeComponent();
            this.InitializeRichInk();

            //Set to Select for a full edit
            this.Pointer = PointerMode.Select;
            
            //Set to Space to prevent user dragging images. If set to something else
            //user may drag images within the control even if ES_READONLY style is applied.            
            //this.Pointer = PointerMode.Space;

            //TODO: Setting some of these properties may cause unexpected display distortion.
            //Perhaps they are meant for the original RichInk control, and not for RichEdit50W that we are using
            /*
            this.View = ViewMode.Writing;
            this.Layer = LayerMode.Drawing;
            this.PageStyle = PageStyleMode.None;
            this.Wrap = WrapMode.Window;
            this.Zoom = 100;
            this.VoiceBar = false;
            */
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Public Methods
        /// <summary>
        /// Paste the data on the clipboard to the Rich Ink control.
        /// </summary>
        /// <remarks>
        /// Return TRUE if OK, FALSE if failed.
        /// Added by MD (29 June 2010)
        /// </remarks>
        public bool PasteFromClipboard()
        {
            //Adapted from http://new.pocketpcdn.com/forum/viewtopic.php?f=6&t=2639 (insert bitmap into RichInk).
            if (!SendMessage(this.m_RichInk.NativeHandle, EM_CANPASTE, 0, 0))
                return false;
            else
                return SendMessage(this.m_RichInk.NativeHandle, WM_PASTE, 0, 0);
        }

        /// <summary>
        /// Insert the specified bitmap to the text. Return FALSE if failed.
        /// </summary>
        public bool InsertImage(Bitmap bmp)
        {
            try
            {
                Clipboard.SetDataObject(bmp);
                return PasteFromClipboard();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception @InsertImage: " + ex.ToString());
                return false;
            }
        }

        public void ChangeBold()
        {
            bool fontBold = this.FontBold;
            this.FontBold = !fontBold;
        }

        public void ChangeBullet()
        {
            bool fontBullet = this.FontBullet;
            this.FontBullet = !fontBullet;
        }

        public void ChangeItalic()
        {
            bool fontItalic = this.FontItalic;
            this.FontItalic = !fontItalic;
        }

        public void ChangeStrikeout()
        {
            bool fontStrikeout = this.FontStrikeout;
            this.FontStrikeout = !fontStrikeout;
        }

        public void ChangeUnderline()
        {
            bool fontUnderline = this.FontUnderline;
            this.FontUnderline = !fontUnderline;
        }

        public void ClearAll()
        {
            WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x607, (uint) 0, (uint) 0);
            this.View = ViewMode.Writing;
            this.Pointer = PointerMode.Select;
            this.Layer = LayerMode.Smart;
            this.PageStyle = PageStyleMode.None;
            this.Wrap = WrapMode.Window;
        }

        public FileFormat GetFileFormat(string file)
        {
            switch (Path.GetExtension(file))
            {
                case ".pwi":
                    return FileFormat.PWI;

                case ".rtf":
                    return FileFormat.RTF;
            }
            return FileFormat.TXT;
        }

        public string GetText()
        {
            StringBuilder builder;
            if (this.m_RichInk.NativeHandle == IntPtr.Zero)
            {
                return base.Text;
            }
            try
            {
                builder = new StringBuilder();
            }
            catch (OutOfMemoryException)
            {
                GC.WaitForPendingFinalizers();
                builder = new StringBuilder();
            }
            int num = 0;
            num = (int)WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 14, IntPtr.Zero, IntPtr.Zero);
            if (num > 0)
            {
                IntPtr ptr = WindowsAPIs.LocalAlloc(0x40, (uint)((num + 1) * 2));
                Message message = Message.Create(this.m_RichInk.NativeHandle, 13, new IntPtr(num + 1), ptr);
                MessageWindow.SendMessage(ref message);
                if (message.Result.ToInt32() > 0)
                {
                    builder.Insert(0, Marshal.PtrToStringUni(ptr));
                }
                WindowsAPIs.LocalFree(ptr);
                builder.Replace("\r", "");
            }
            return builder.ToString();
        }

        public bool IsSelected()
        {
            int wParam = 0;
            int lParam = 0;
            int num3 = 0;
            int num4 = (int)WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0xb0, ref wParam, ref lParam);
            if ((lParam - wParam) > 0)
            {
                num3 = lParam - wParam;
            }
            return (num3 != 0);
        }

        /// <summary>
        /// Read the specified file. Supported file format: RTF, PWI, TXT.
        /// </summary>
        /// <param name="file"></param>
        /// <param name="fileformat"></param>
        /// <remarks>
        /// Not all features of RTF are supported. In fact, embedded images cannot be displayed properly.
        /// Embedded images in PWI can be displayed, however, PWI converted from RTF using Microsoft Word
        /// has very low image quality and is almost unusabe.
        /// Microsoft Office Word Mobile suffers from the same problem.
        /// </remarks>
        public void ReadFile(string file, FileFormat fileformat)
        {
            uint num;
            Cursor.Current = Cursors.WaitCursor;
            EditStream structure = new EditStream();
            Cookie cookie = new Cookie();
            fsize = 0;
            fstream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read);
            fsize = (int)fstream.Length;
            switch (fileformat)
            {
                case FileFormat.RTF:
                    num = 2;
                    break;

                case FileFormat.PWI:
                    num = 0x10802;
                    break;

                case FileFormat.TXT:
                    num = 1;
                    break;

                default:
                    throw new ArgumentOutOfRangeException("Unknown File Format.");
            }
            buff = new byte[fsize];
            fstream.Read(buff, 0, fsize);
            IntPtr ptr = WindowsAPIs.LocalAlloc(0x40, (uint)Marshal.SizeOf(cookie));
            cookie.dwError = 0;
            cookie.pbStart = IntPtr.Zero;
            cookie.pbCur = cookie.pbStart;
            cookie.bCount = fsize;
            Marshal.StructureToPtr(cookie, ptr, false);
            structure.dwCookie = ptr;
            structure.dwError = 0;
            fpos = 0;
            EditStreamCallback d = new EditStreamCallback(this.EditStreamReadCallback);
            structure.pfnCallback = Marshal.GetFunctionPointerForDelegate(d);
            IntPtr ptr2 = WindowsAPIs.LocalAlloc(0x40, (uint)Marshal.SizeOf(structure));
            Marshal.StructureToPtr(structure, ptr2, false);
            WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x449, num, ptr2);
            WindowsAPIs.LocalFree(ptr2);
            WindowsAPIs.LocalFree(ptr);
            fstream.Close();
            fstream = null;
            Cursor.Current = Cursors.Default;
        }

        public void Redo()
        {
            WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4eb, (uint)0, (uint)0);
        }

        public void SelectWordAtPos()
        {
            int wParam = 0;
            int lParam = 0;
            int length = 0;
            int num4 = 0;
            num4 = (int)WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0xb0, ref wParam, ref lParam);
            string text = this.GetText();
            length = text.Length;
            int num5 = lParam;
            wParam = num5;
            if (num5 < length)
            {
                int num6;
                for (num6 = num5; num6 > 0; num6--)
                {
                    if ((char.IsWhiteSpace(text[num6]) || char.IsPunctuation(text[num6])) || char.IsSurrogate(text[num6]))
                    {
                        break;
                    }
                    wParam--;
                }
                if (wParam == 0)
                {
                    wParam = -1;
                }
                lParam = num5;
                for (num6 = num5; num6 < (length - 1); num6++)
                {
                    if ((char.IsWhiteSpace(text[num6]) || char.IsPunctuation(text[num6])) || char.IsSurrogate(text[num6]))
                    {
                        break;
                    }
                    lParam++;
                }
                if (lParam > wParam)
                {
                    num4 = (int)WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0xb1, (uint)(wParam + 1), (uint)lParam);
                }
            }
        }

        public void Undo()
        {
            WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4ea, (uint)0, (uint)0);
        }

        public void WriteFileAs(string file, FileFormat fileformat)
        {
            uint num;
            Cursor.Current = Cursors.WaitCursor;
            EditStream structure = new EditStream();
            Cookie cookie = new Cookie();
            fsize = 0;
            fstream = File.Create(file);
            switch (fileformat)
            {
                case FileFormat.RTF:
                    num = 2;
                    break;

                case FileFormat.PWI:
                    num = 0x10802;
                    break;

                case FileFormat.TXT:
                    num = 1;
                    break;

                default:
                    throw new ArgumentOutOfRangeException("Unknown File Format.");
            }
            IntPtr ptr = WindowsAPIs.LocalAlloc(0x40, (uint)Marshal.SizeOf(cookie));
            cookie.hFile = 0;
            cookie.dwError = 0;
            cookie.pbStart = IntPtr.Zero;
            cookie.pbCur = cookie.pbStart;
            cookie.bCount = 0L;
            Marshal.StructureToPtr(cookie, ptr, false);
            structure.dwCookie = ptr;
            structure.dwError = 0;
            EditStreamCallback d = new EditStreamCallback(this.EditStreamSizeCallback);
            structure.pfnCallback = Marshal.GetFunctionPointerForDelegate(d);
            IntPtr ptr2 = WindowsAPIs.LocalAlloc(0x40, (uint)Marshal.SizeOf(structure));
            Marshal.StructureToPtr(structure, ptr2, false);
            WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x44a, num, ptr2);
            WindowsAPIs.LocalFree(ptr2);
            WindowsAPIs.LocalFree(ptr);
            buff = new byte[fsize];
            cookie.pbStart = IntPtr.Zero;
            cookie.pbCur = cookie.pbStart;
            cookie.bCount = fsize;
            cookie.dwError = 0;
            Marshal.StructureToPtr(cookie, ptr, false);
            structure.dwCookie = ptr;
            structure.dwError = 0;
            fpos = 0;
            EditStreamCallback callback2 = new EditStreamCallback(RichInkControl.EditStreamWriteCallback);
            structure.pfnCallback = Marshal.GetFunctionPointerForDelegate(callback2);
            ptr2 = WindowsAPIs.LocalAlloc(0x40, (uint)Marshal.SizeOf(structure));
            Marshal.StructureToPtr(structure, ptr2, false);
            WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x44a, num, ptr2);
            WindowsAPIs.LocalFree(ptr2);
            WindowsAPIs.LocalFree(ptr);
            fstream.Write(buff, 0, fsize);
            fstream.Close();
            fstream = null;
            this.IsModified = false;
            Cursor.Current = Cursors.Default;
        }
        #endregion

        #region Private Methods
        private uint EditStreamReadCallback(ref Cookie cookie, IntPtr pbBuff, int cb, out int transferred)
        {
            if (cb > (fsize - fpos))
            {
                cb = fsize - fpos;
            }
            Marshal.Copy(buff, fpos, pbBuff, cb);
            fpos += cb;
            transferred = cb;
            return 0;
        }

        private uint EditStreamSizeCallback(ref Cookie cookie, IntPtr pbBuff, int cb, out int transferred)
        {
            fsize += cb;
            transferred = cb;
            return 0;
        }

        public static uint EditStreamWriteCallback(ref Cookie cookie, IntPtr pbBuff, int cb, out int transferred)
        {
            if (cb > 0)
            {
                Marshal.Copy(pbBuff, buff, fpos, cb);
            }
            fpos += cb;
            transferred = cb;
            return 0;
        }

        private void InitializeComponent()
        {
            base.SuspendLayout();
            //base.AutoScaleDimensions = new SizeF(96f, 96f);
            base.AutoScaleMode = AutoScaleMode.Dpi;
            //this.BackColor = Color.FromArgb(0x80, 0xff, 0xff);
            base.Name = "RichInkControl";
            //base.Size = new Size(0xb1, 170);
            base.ResumeLayout(false);
        }

        private static bool IsLibraryLoaded = false;

        //create a richink Win32 window
        private void InitializeRichInk()
        {
            InitInkX();

            /*
             MD: The default RickInk/InkX control does not support
             many of the advanced RTF capability (e.g. tables, inline images)
             so RTF will be poorly displayed. We need to use the window
             class RICHEDIT50W same as Pocket Word in order to support
             these advanced features.
             */

            //the window class is in this library so load it
            if (!IsLibraryLoaded)
            {
                WindowsAPIs.LoadLibrary("RichEd20.dll");
                IsLibraryLoaded = true;
            }

            //RichEdit50 - full edit            
            //this.m_RichInk = new WindowHost("RICHEDIT50W", 0, 0);

            //RichEdit50 - Read Only
            this.m_RichInk = new WindowHost("RICHEDIT50W", 0, ES_READONLY);

            //RichInk - Original Code
            //this.m_RichInk = new WindowHost("RichInk", 0, ES_READONLY);

            this.m_RichInk.Dock = DockStyle.Fill;
            this.m_RichInk.Parent = this;
        }
        #endregion

        #region DllImport
        private const int SB_TOP = 6; 
        private const int WM_VSCROLL = 0x115; 
        private const int EM_CANPASTE = 1074;
        private const int WM_PASTE = 0x302;
        private const int ES_READONLY = 0x800;
        private const int EM_SETREADONLY = 0xCF;

        [DllImport("inkx.dll")]
        private static extern void InitInkX();

        [DllImport("coredll.dll")]
        private static extern int SendMessage(IntPtr hWnd, uint message, IntPtr wParam, uint lParam);

        [DllImport("coredll.dll")]
        private static extern bool SendMessage(IntPtr hWnd, uint message, uint wParam, uint lParam);
        #endregion

        #region Public Propeties
        public ActionMode Action
        {
            get
            {
                return this.m_Action;
            }
            set
            {
                this.m_Action = value;
                switch (value)
                {
                    case ActionMode.Smart:
                        this.View = ViewMode.Typing;
                        this.Pointer = PointerMode.Select;
                        this.Layer = LayerMode.Smart;
                        break;

                    case ActionMode.Writing:
                        this.View = ViewMode.Typing;
                        this.Pointer = PointerMode.Select;
                        this.Layer = LayerMode.Writing;
                        break;

                    case ActionMode.Drawing:
                        this.View = ViewMode.Drawing;
                        this.Pointer = PointerMode.Pen;
                        this.Layer = LayerMode.Drawing;
                        break;

                    default:
                        throw new ArgumentOutOfRangeException("Unknown Action Mode.");
                }
            }
        }

        public InkFontColor FillColor
        {
            get
            {
                IntPtr lParam = WindowsAPIs.LocalAlloc(0x40, 0x80);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d4, (uint) 0, lParam);
                byte num = Marshal.ReadByte(lParam, 0x4a);
                WindowsAPIs.LocalFree(lParam);
                return (InkFontColor) num;
            }
            set
            {
                IntPtr lParam = WindowsAPIs.LocalAlloc(0x40, 0x80);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d4, (uint) 0, lParam);
                byte val = (byte) (Marshal.ReadByte(lParam, 0x7a) | 2);
                Marshal.WriteByte(lParam, 0x4a, (byte) value);
                Marshal.WriteByte(lParam, 80, 1);
                Marshal.WriteByte(lParam, 0x7a, val);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d5, (uint) 0, lParam);
                WindowsAPIs.LocalFree(lParam);
            }
        }

        public InkFontColor FillColorNone
        {
            get
            {
                IntPtr lParam = WindowsAPIs.LocalAlloc(0x40, 0x80);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d4, (uint) 0, lParam);
                byte num = Marshal.ReadByte(lParam, 0x4a);
                WindowsAPIs.LocalFree(lParam);
                return (InkFontColor) num;
            }
            set
            {
                IntPtr lParam = WindowsAPIs.LocalAlloc(0x40, 0x80);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d4, (uint) 0, lParam);
                byte val = (byte) (Marshal.ReadByte(lParam, 0x7a) | 2);
                Marshal.WriteByte(lParam, 0x4a, (byte) value);
                Marshal.WriteByte(lParam, 80, 0);
                Marshal.WriteByte(lParam, 0x7a, val);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d5, (uint) 0, lParam);
                WindowsAPIs.LocalFree(lParam);
            }
        }

        public bool FontBold
        {
            get
            {
                IntPtr lParam = WindowsAPIs.LocalAlloc(0x40, 0x80);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d4, (uint) 0, lParam);
                byte num = Marshal.ReadByte(lParam, 0x4c);
                WindowsAPIs.LocalFree(lParam);
                return (num == 7);
            }
            set
            {
                IntPtr lParam = WindowsAPIs.LocalAlloc(0x40, 0x80);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d4, (uint) 0, lParam);
                byte num = Marshal.ReadByte(lParam, 0x4c);
                byte val = Marshal.ReadByte(lParam, 0x7a);
                if (value)
                {
                    Marshal.WriteByte(lParam, 0x4c, 7);
                    Marshal.WriteByte(lParam, 0x55, 8);
                }
                else
                {
                    Marshal.WriteByte(lParam, 0x4c, 1);
                    Marshal.WriteByte(lParam, 0x55, 0);
                }
                val = (byte) (val | 4);
                Marshal.WriteByte(lParam, 0x7a, val);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d5, (uint) 0, lParam);
                WindowsAPIs.LocalFree(lParam);
            }
        }

        public bool FontBullet
        {
            get
            {
                IntPtr lParam = WindowsAPIs.LocalAlloc(0x40, 0x80);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d4, (uint) 0, lParam);
                byte num = Marshal.ReadByte(lParam, 90);
                WindowsAPIs.LocalFree(lParam);
                return (num == 0xff);
            }
            set
            {
                IntPtr lParam = WindowsAPIs.LocalAlloc(0x40, 0x80);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d4, (uint) 0, lParam);
                byte val = Marshal.ReadByte(lParam, 90);
                byte num2 = (byte) (Marshal.ReadByte(lParam, 0x79) | 0x7f);
                if (value)
                {
                    val = 0xff;
                }
                else
                {
                    val = 0;
                }
                Marshal.WriteByte(lParam, 90, val);
                Marshal.WriteByte(lParam, 0x79, num2);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d5, (uint) 0, lParam);
                WindowsAPIs.LocalFree(lParam);
            }
        }

        public InkFontColor FontColor
        {
            get
            {
                IntPtr lParam = WindowsAPIs.LocalAlloc(0x40, 0x80);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d4, (uint) 0, lParam);
                byte num = Marshal.ReadByte(lParam, 0x48);
                WindowsAPIs.LocalFree(lParam);
                return (InkFontColor) num;
            }
            set
            {
                IntPtr lParam = WindowsAPIs.LocalAlloc(0x40, 0x80);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d4, (uint) 0, lParam);
                byte val = (byte) (Marshal.ReadByte(lParam, 0x7a) | 1);
                Marshal.WriteByte(lParam, 0x48, (byte) value);
                Marshal.WriteByte(lParam, 0x7a, val);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d5, (uint) 0, lParam);
                WindowsAPIs.LocalFree(lParam);
            }
        }

        public bool FontItalic
        {
            get
            {
                IntPtr lParam = WindowsAPIs.LocalAlloc(0x40, 0x80);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d4, (uint) 0, lParam);
                byte num = Marshal.ReadByte(lParam, 0x54);
                WindowsAPIs.LocalFree(lParam);
                return ((num & 8) == 8);
            }
            set
            {
                IntPtr lParam = WindowsAPIs.LocalAlloc(0x40, 0x80);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d4, (uint) 0, lParam);
                byte val = Marshal.ReadByte(lParam, 0x54);
                byte num2 = Marshal.ReadByte(lParam, 120);
                val = (byte) (val & 0xf7);
                if (value)
                {
                    val = (byte) (val + 8);
                }
                num2 = (byte) (num2 | 8);
                Marshal.WriteByte(lParam, 0x54, val);
                Marshal.WriteByte(lParam, 120, num2);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d5, (uint) 0, lParam);
                WindowsAPIs.LocalFree(lParam);
            }
        }

        public bool FontStrikeout
        {
            get
            {
                IntPtr lParam = WindowsAPIs.LocalAlloc(0x40, 0x80);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d4, (uint) 0, lParam);
                byte num = Marshal.ReadByte(lParam, 0x54);
                WindowsAPIs.LocalFree(lParam);
                return ((num & 1) == 1);
            }
            set
            {
                IntPtr lParam = WindowsAPIs.LocalAlloc(0x40, 0x80);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d4, (uint) 0, lParam);
                byte val = Marshal.ReadByte(lParam, 0x54);
                byte num2 = Marshal.ReadByte(lParam, 120);
                val = (byte) (val & 0xfe);
                if (value)
                {
                    val = (byte) (val + 1);
                }
                num2 = (byte) (num2 | 1);
                Marshal.WriteByte(lParam, 0x54, val);
                Marshal.WriteByte(lParam, 120, num2);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d5, (uint) 0, lParam);
                WindowsAPIs.LocalFree(lParam);
            }
        }

        public bool FontUnderline
        {
            get
            {
                IntPtr lParam = WindowsAPIs.LocalAlloc(0x40, 0x80);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d4, (uint) 0, lParam);
                byte num = Marshal.ReadByte(lParam, 0x54);
                WindowsAPIs.LocalFree(lParam);
                return ((num & 2) == 2);
            }
            set
            {
                IntPtr lParam = WindowsAPIs.LocalAlloc(0x40, 0x80);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d4, (uint) 0, lParam);
                byte val = Marshal.ReadByte(lParam, 0x54);
                byte num2 = Marshal.ReadByte(lParam, 120);
                val = (byte) (val & 0xfd);
                if (value)
                {
                    val = (byte) (val + 2);
                }
                num2 = (byte) (num2 | 2);
                Marshal.WriteByte(lParam, 0x54, val);
                Marshal.WriteByte(lParam, 120, num2);
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x4d5, (uint) 0, lParam);
                WindowsAPIs.LocalFree(lParam);
            }
        }

        public bool IsModified
        {
            get
            {
                return SendMessage(this.m_RichInk.NativeHandle, 0xb8, (uint) 0, 0);
            }
            set
            {
                if (!value)
                {
                    WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0xb9, (uint) 0, (uint) 0);
                }
                else
                {
                    WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0xb9, (uint) 1, (uint) 0);
                }
            }
        }

        public LayerMode Layer
        {
            get
            {
                return this.m_Layer;
            }
            set
            {
                this.m_Layer = value;
                int num = 0;
                switch (value)
                {
                    case LayerMode.Writing:
                        num = 1;
                        break;

                    case LayerMode.Drawing:
                        num = 2;
                        break;

                    case LayerMode.Smart:
                        num = 0;
                        break;

                    case LayerMode.Links:
                        num = 3;
                        break;

                    case LayerMode.SmartLinks:
                        num = 4;
                        break;

                    default:
                        throw new ArgumentOutOfRangeException("Unknown LayerMode.");
                }
                if ((num != -1) && (this.m_RichInk.NativeHandle != IntPtr.Zero))
                {
                    WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x520, Convert.ToUInt32(num), (uint) 0);
                }
            }
        }

        public PageStyleMode PageStyle
        {
            get
            {
                return this.m_PageStyle;
            }
            set
            {
                this.m_PageStyle = value;
                int num = 0;
                switch (value)
                {
                    case PageStyleMode.Yellow:
                        num = 0x40;
                        break;

                    case PageStyleMode.DottedLines:
                        num = 0x20;
                        break;

                    case PageStyleMode.GridLines:
                        num = 4;
                        break;

                    case PageStyleMode.RuledLines:
                        num = 2;
                        break;

                    case PageStyleMode.TopMargin:
                        num = 1;
                        break;

                    case PageStyleMode.LeftMargin:
                        num = 0;
                        break;

                    case PageStyleMode.TopLeftMargin:
                        num = 8;
                        break;

                    case PageStyleMode.None:
                        num = 0x10;
                        break;

                    default:
                        throw new ArgumentOutOfRangeException("Unknown PageStyleMode.");
                }
                if ((num != -1) && (this.m_RichInk.NativeHandle != IntPtr.Zero))
                {
                    WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x51f, Convert.ToUInt32(num), (uint) 0);
                }
            }
        }

        public PointerMode Pointer
        {
            get
            {
                return this.m_Pointer;
            }
            set
            {
                this.m_Pointer = value;
                int num = 0;
                switch (value)
                {
                    case PointerMode.Select:
                        num = 1;
                        break;

                    case PointerMode.Pen:
                        num = 0;
                        break;

                    case PointerMode.Space:
                        num = 2;
                        break;

                    default:
                        throw new ArgumentOutOfRangeException("Unknown PointerMode.");
                }
                if ((num != -1) && (this.m_RichInk.NativeHandle != IntPtr.Zero))
                {
                    WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x549, Convert.ToUInt32(num), (uint) 0);
                }
            }
        }

        public ViewMode View
        {
            get
            {
                switch (SendMessage(this.m_RichInk.NativeHandle, 0x4fe, IntPtr.Zero, 0))
                {
                    case 0:
                        this.m_View = ViewMode.Typing;
                        break;

                    case 2:
                        this.m_View = ViewMode.Writing;
                        break;

                    case 3:
                        this.m_View = ViewMode.Drawing;
                        break;

                    default:
                        throw new ArgumentOutOfRangeException("Unknown ViewMode.");
                }
                return this.m_View;
            }
            set
            {
                this.m_View = value;
                int num = 0;
                switch (value)
                {
                    case ViewMode.Typing:
                        num = 0;
                        break;

                    case ViewMode.Writing:
                        num = 2;
                        break;

                    case ViewMode.Drawing:
                        num = 3;
                        break;

                    default:
                        throw new ArgumentOutOfRangeException("Unknown ViewMode.");
                }
                if ((num != -1) && (this.m_RichInk.NativeHandle != IntPtr.Zero))
                {
                    WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x51c, Convert.ToUInt32(num), (uint) 0);
                }
            }
        }

        public bool VoiceBar
        {
            get
            {
                return this.m_VoiceBar;
            }
            set
            {
                this.m_VoiceBar = value;
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x612, Convert.ToUInt32(this.m_VoiceBar), (uint) 0);
            }
        }

        public WrapMode Wrap
        {
            get
            {
                switch (SendMessage(this.m_RichInk.NativeHandle, 0x4e3, IntPtr.Zero, 0))
                {
                    case 0:
                        this.m_Wrap = WrapMode.Page;
                        break;

                    case 1:
                        this.m_Wrap = WrapMode.Window;
                        break;

                    default:
                        throw new ArgumentOutOfRangeException("Unknown WrapMode.");
                }
                return this.m_Wrap;
            }
            set
            {
                this.m_Wrap = value;
                int num = 0;
                switch (value)
                {
                    case WrapMode.Window:
                        num = 1;
                        break;

                    case WrapMode.Page:
                        num = 0;
                        break;

                    default:
                        throw new ArgumentOutOfRangeException("Unknown WrapMode.");
                }
                if ((num != -1) && (this.m_RichInk.NativeHandle != IntPtr.Zero))
                {
                    WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x53f, Convert.ToUInt32(num), (uint) 0);
                }
            }
        }

        public int Zoom
        {
            get
            {
                return this.m_Zoom;
            }
            set
            {
                this.m_Zoom = value;
                WindowsAPIs.SendMessage(this.m_RichInk.NativeHandle, 0x522, 0, Convert.ToUInt32(value));
            }
        }
        #endregion

        #region Public Data Types
        [StructLayout(LayoutKind.Sequential)]
        public struct Cookie
        {
            public int hFile;
            public IntPtr pbStart;
            public IntPtr pbCur;
            public long bCount;
            public uint dwError;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct EditStream
        {
            public IntPtr dwCookie;
            public int dwError;
            public IntPtr pfnCallback;
        }

        public delegate uint EditStreamCallback(ref RichInkControl.Cookie cookie, IntPtr pbBuff, int cb, out int transferred);

        public enum FileFormat
        {
            RTF,
            PWI,
            TXT
        }
        #endregion
    }
}

