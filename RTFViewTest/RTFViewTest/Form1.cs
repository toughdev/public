﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Net.Sgoliver.NRtfTree.Util;
using System.IO;
using System.Reflection;
using System.Diagnostics;

namespace RTFViewTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void mnuExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void mnuReadRTF_Click(object sender, EventArgs e)
        {
            if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                this.richInkControl1.ReadFile(this.openFileDialog1.FileName, AgileNotesTouch.RichInkControl.FileFormat.RTF);
            }
        }

        private void mnuWriteRTF_Click(object sender, EventArgs e)
        {
            if (this.saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                this.richInkControl1.WriteFileAs(this.saveFileDialog1.FileName, AgileNotesTouch.RichInkControl.FileFormat.RTF);
            }
        }

        private void mnuTestRTF_Click(object sender, EventArgs e)
        {
            string filename = getAppDir() + @"\temp.rtf";

            RtfDocument doc = new RtfDocument(filename, Encoding.UTF8);

            RtfTextFormat format = new RtfTextFormat();
            format.size = 20;
            format.bold = true;
            format.underline = true;
            format.color = Color.Red;
            doc.AddText("Test RTF Document", format);
            doc.AddNewLine();

            format.size = 12;
            format.bold = false;
            format.underline = false;
            format.color = Color.Blue;
            doc.AddText("This is a test.", format);
            doc.AddBitmap(getAppDir() + @"\test.bmp", 100, 34, 4);
            doc.AddText("()[]\\<>?~!@#$%^^*&*()\\\\/;'{}:\"<>/", format);
            doc.AddNewLine();

            format.size = 14;
            format.bold = true;
            format.color = Color.Purple;
            doc.AddText("Vietnamese characters: ôđọcổôgà", format);
            doc.AddNewLine();

            format.size = 12;
            format.italic = true;
            format.color = Color.Gray;
            doc.AddText("Chinese characters: 深知您对我们寄予的信任，以及我们保护您隐私权的责任");
            doc.AddBitmap(getAppDir() + @"\test.bmp", 100, 34, 24);

            doc.Close();

            this.richInkControl1.ReadFile(filename, AgileNotesTouch.RichInkControl.FileFormat.RTF);

            File.Delete(filename);
        }

        /// <summary>
        /// Get the application directory.
        /// </summary>
        public static string getAppDir()
        {
            return Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);
        }
    }
}