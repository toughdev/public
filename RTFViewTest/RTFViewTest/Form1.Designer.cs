﻿namespace RTFViewTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.mnuExit = new System.Windows.Forms.MenuItem();
            this.mnuOptions = new System.Windows.Forms.MenuItem();
            this.mnuReadRTF = new System.Windows.Forms.MenuItem();
            this.mnuWriteRTF = new System.Windows.Forms.MenuItem();
            this.mnuTestRTF = new System.Windows.Forms.MenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.richInkControl1 = new AgileNotesTouch.RichInkControl();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.mnuExit);
            this.mainMenu1.MenuItems.Add(this.mnuOptions);
            // 
            // mnuExit
            // 
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuOptions
            // 
            this.mnuOptions.MenuItems.Add(this.mnuReadRTF);
            this.mnuOptions.MenuItems.Add(this.mnuWriteRTF);
            this.mnuOptions.MenuItems.Add(this.mnuTestRTF);
            this.mnuOptions.Text = "Options";
            // 
            // mnuReadRTF
            // 
            this.mnuReadRTF.Text = "Read RTF";
            this.mnuReadRTF.Click += new System.EventHandler(this.mnuReadRTF_Click);
            // 
            // mnuWriteRTF
            // 
            this.mnuWriteRTF.Text = "Write RTF";
            this.mnuWriteRTF.Click += new System.EventHandler(this.mnuWriteRTF_Click);
            // 
            // mnuTestRTF
            // 
            this.mnuTestRTF.Text = "Test RTF";
            this.mnuTestRTF.Click += new System.EventHandler(this.mnuTestRTF_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "test.rtf";
            this.openFileDialog1.Filter = "Rich Text Document|*.rtf";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileName = "test.rtf";
            this.saveFileDialog1.Filter = "Rich Text Document|*.rtf";
            // 
            // richInkControl1
            // 
            this.richInkControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richInkControl1.Location = new System.Drawing.Point(0, 0);
            this.richInkControl1.Name = "richInkControl1";
            this.richInkControl1.Size = new System.Drawing.Size(240, 268);
            this.richInkControl1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.ControlBox = false;
            this.Controls.Add(this.richInkControl1);
            this.Menu = this.mainMenu1;
            this.Name = "Form1";
            this.Text = "Test RTF Viewer";
            this.ResumeLayout(false);

        }

        #endregion

        private AgileNotesTouch.RichInkControl richInkControl1;
        private System.Windows.Forms.MenuItem mnuExit;
        private System.Windows.Forms.MenuItem mnuOptions;
        private System.Windows.Forms.MenuItem mnuReadRTF;
        private System.Windows.Forms.MenuItem mnuWriteRTF;
        private System.Windows.Forms.MenuItem mnuTestRTF;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

